# TWSS

This project implements a classifier that decides if a phrase can be replied
with "That's what she said".

## Setup


#### Python

Clone the project from GitLab

```shell
git clone git@gitlab.com:susuhahnml/twss.git
```
Make sure you have python installed.

```shell
python --version
```

We will be using the version 3.7 of python. Make sure this is your current version, if not, update it.

#### Requirments

Install if necessary requirements by running the following command inside the project directory.

```shell
pip install -r requirements.txt
```

Make sure to have the lastest `pip` version. In some cases might be run as `pip3`.

To install the language information for parsing use:

```shell
python -m spacy download en
```
Note: In some cases you might have to use `python3` fo the appropoate version.

If using a **Linux Distribution**, the installation of `python-dev` is also requiered.


## Run

The branch `master` has the last stable version of the code.

The main file will:

1) Create a Featurizer
2) Featurize the corpus, or use the one saved.
3) Divide the corpus for train and test
4) Train the model using cross validation and grid search for the best parameters, or use the one saved
5) Enter an intetactive menu with the following functionalities:
  - Evaluate with test data
  - Enter phrases and see the prediction
  - Generate probabilites for unanotated files
  - Print weights
  - Print odd ratio of features(Just Logistc Regretion)
  - Python console to interact with variabls
  - Print clasification data for sentence


Standig on the root of the project run:

```shell
python src/main.py model-name
```

The options for `model-name` are: `logistic-regression`, `multinomial-bayes` and `support-vector-machine`

### Retrain

To force to featurize corpus and train model without using saved data use:

```shell
python src/main.py model-name recalculate
```

This option will take longer than using the saved models.

Note: With this option the models will be retrained. However this will train only using the
best hyperparameters. In order to do a full grid search, the corresponding creation files
must be modified as indicated in each file.


## Tests

For the test we use the pytest pakage, the documentation can be found [here](https://docs.pytest.org/en/latest/getting-started.html#install-pytest).

To run all tests use the following command

```shell
pytest . -v -s -p no:warnings
```

To run tests in one file run

```shell
pytest src/tests/test_file.py -v -s
```

This will run the tests in all the files in the tests folder.
All test files must have the format `test_*.py`

## Telegram bot

This project also includes the code to run a telegram bot called @thatswhatss_bot.

You can interact with him and add him to your telegram groups.

To run the bot code use:

```shell
python3 src/telegram_bot.py
```
Note: This code is already runnign on a server so that is always available.


## Project structure

```
twss
│   
├───README.md               Markdown file with documentation.
├───requirements.txt        List of packages to be atomaticly installed
│
├───corpus                  Corpus related files
│   │
│   ├───clean_data          The corpus cleaned
│   │
│   ├───negative            The negative corpus not cleaned
│   │
│   ├───positive            The positive corpus not cleaned
│   │
│   ├───probabilities       The probabilities of unannotated files
│   │
│   ├───raw_data            The raw data scraped from the web
│   │
│   ├───unannotated         The unannotated corpus used for human evaluation
│   │
│   ├───erotica_frecuencies_processed.json         The json with the final erotica frecuencies
│   └───erotica_frecuencies_original.json          The json with the original erotica frecuencies
│
└───src                     Source files
    │
    ├───classifier          Code for the different classifiers
    │   │
    │   └───saved           The saved classifiers
    │
    ├───features            Code for the different features
    │   │
    │   ├───feature_*.py    A specific feature
    │   └───featurizer.py   The general featurizer
    │
    ├───postprocessing      Code analysis of evaluation data
    │
    ├───preprocessing       Code to process data before classification (Cleaning, erotica and scraping)
    │
    ├───tests               Tests files
    │
    ├───main.py             Main file to run classifiers
    └───telegram_bot.py     Code to run telegram bot

```

## Git Workflow

The lists of issues pending to be implemented can be found inside the platform, in the GitLab Issues Board.
All issues will be integrated to the code via MargeRequests as follows.

### Start a new issue

1. Make sure you have the last changes.

```shell
git checkout master
git pull
```

2. Create a new branch named after your issue. Using the number of the issue as prefix to link it to the board. Example `10-positive-corpus`

```shell
git checkout -b branch-name
```

3. Implement your code in the branch you created.

4. Commit your changes

```shell
git add .
git commit -m "Short description of what your changes do"
```
5. Push your branch into the git history

```shell
git push origin branch-name
```

### Submit branch to a merge request

To submit your branch to be added to master you must go to the platform and create a new MergeRequest using the `master` branch
as the objective branch.

If your work is still in process then make sure to add the prefix
`WIP:` to the name of the MergeRequest.

### Accept a MergeRequest

Once a merge request has been properly reviewed, it can be merged into the master branch though the platform. All conflicts must be resolved in the process.

#### Solving conflicts

If a branch is not up to date with the master branch, meaning that there is new work in master that was not when you started working. You must rebase your branch so that it includes all this changes before.

Once you have uploaded you work as mentioned in point 5, do the following.

1. Get all the new history
```shell
git checkout master
git pull origin master
```

2. Move back to your branch
```shell
git checkout branch-name
```

3. Rebase the master branch
```shell
git rebase master
```

Any conflicts must be manually solved and follow the git instructions to finish the rebase.

4. Push your branch with the new changes
```shell
git push origin branch-name -f
```

The `-f` command implies a forced push, this is necessary because we are rewriting the current history of our branch.
