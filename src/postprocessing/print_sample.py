import csv
from io import StringIO
from collections import defaultdict
import random
"""
Script to get a sample for the output probability files.
The samples are saved in corresponing files
"""


filnames = [
'../../corpus/probabilities/ClassifierLogisticRegression_probabilies_sausage_party.txt.csv',
'../../corpus/probabilities/ClassifierLogisticRegression_probabilies_lion_king.txt.csv',
'../../corpus/probabilities/ClassifierLogisticRegression_probabilies_cinderella.txt.csv']

for filename in filnames:
	all_texts = []

	with open(filename, 'rt') as f:
	    data = f.read()
	    data = data.replace('\x00','')
	    reader = csv.DictReader(StringIO(data), delimiter='\t')
	    for row in reader:
	        all_texts.append([row['twss-probability'], row['sentence']])
	percs = range(0,100,5)
	samples = defaultdict()
	for perc in percs:
		subset = [entry[1] for entry in all_texts if float(entry[0]) >= float(perc/100) and float(entry[0]) < float((perc/100)+0.05)]
		if len(subset)>=5:
			samples[perc] = random.sample(subset,5)
		else:
			samples[perc] = subset
	string_list=[]
	for key in samples.keys():
		heading = "+++++"+str(key)+"-"+str(key+5)+"++++"
		string_list.append(heading)
		for elem in samples[key]:
			string_list.append(elem)
	filepath = "../../corpus/probabilities/sample_"+filename.split('/')[-1].replace('ClassifierLogisticRegression_probabilies_', "").replace('.csv', '')
	with open(filepath, 'w') as f:
		for elem in string_list:
			f.write("%s\n" % elem)
