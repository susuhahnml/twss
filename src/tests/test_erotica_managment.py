from preprocessing import erotica_managment
erotica_file_path = 'src/tests/files/erotica_test.txt'

def test_pos_frecuency():
    frecuencydict = erotica_managment.pos_frecuency(['VERB','ADJ','NOUN'],erotica_file_path)


def test_erotica_managment():
    erotica_managment.process_sentences_to_json_file('src/tests/files/erotica_test.txt','src/tests/files/erotica_test_out.json')
    processed = erotica_managment.read_json_file('src/tests/files/erotica_test_out.json')
    assert "VERB" in processed

def test_erotica_managment_filter():
    test_dict={"NOUN":{"dick":1,"dicks":3}}
    new = erotica_managment.set_minimum_frecuency(2)(test_dict)
    assert new["NOUN"]["dicks"]==3
    assert not "dick" in new["NOUN"]

def test_lematize():
    test_dict={"NOUN":{"dick":1,"dicks":3}}
    processed= erotica_managment.lematize(test_dict)
    assert processed["NOUN"]["dick"]==4

def test_sort_dict():
    test_dict={"NOUN":{"dick":1,"boobs":4,"dicks":3}}
    processed = erotica_managment.sort_dict_erotica(test_dict)
    print(processed)

def test_erotica_managment_filter():
    test_dict={"NOUN":{"dick":10,"dicks":33}}
    new = erotica_managment.divide_frecuencies(10)(test_dict)
    assert new["NOUN"]["dick"]==1
    assert new["NOUN"]["dicks"]==3
