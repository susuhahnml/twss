from features import FeatureWordsNumber

def test_featurize_words_number():
    feature = FeatureWordsNumber()
    list = feature.featurize("Hello world")
    assert len(list)==1
    assert list[0]==2

def test_length_get_info():
    feature = FeatureWordsNumber()
    features_list = [12]
    info = feature.get_info(features_list)
    assert info["number_of_words"]==12
    print(feature.get_info(features_list))
