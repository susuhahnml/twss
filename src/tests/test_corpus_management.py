from preprocessing import corpus_management
from preprocessing import corpus_management
from features import FeatureWordsNumber

negative_file_path = 'src/tests/files/negative_test.txt'
positive_file_path = 'src/tests/files/positive_test.txt'

def test_combine_corpora():
    all_files = [positive_file_path, negative_file_path]
    combined_corpora = corpus_management.combine_corpora(all_files)
    #print(combined_corpora[:10])
    assert type(combined_corpora) == list

def test_featurize_corpora():
    positive_files = [positive_file_path]
    negative_files = [negative_file_path]
    featurizer = FeatureWordsNumber()
    c = corpus_management.featurize_corpus(positive_files, negative_files, featurizer, True, balance_corpora = False)
    assert c[positive_file_path]['Y'][0]==1
    assert c[negative_file_path]['Y'][0]==0
    assert c[positive_file_path]['X'][0]==[3]
    assert c[negative_file_path]['X'][0]==[2]

def test_featurize_corpora_unbalanced():
    positive_files = [positive_file_path]
    negative_files = [negative_file_path]
    featurizer = FeatureWordsNumber()
    c = corpus_management.featurize_corpus(positive_files, negative_files, featurizer, True, balance_corpora = False)
    assert c[positive_file_path]['Y'][0]==1
    assert c[negative_file_path]['Y'][0]==0
    assert c[positive_file_path]['X'][0]==[3]
    assert c[negative_file_path]['X'][0]==[2]

def test_featurize_corpora_balanced():
    positive_files = [positive_file_path]
    negative_files = [negative_file_path]
    featurizer = FeatureWordsNumber()
    c = corpus_management.featurize_corpus(positive_files, negative_files, featurizer, True)
    assert c[positive_file_path]['Y'][0]==1
    assert c[negative_file_path]['Y'][0]==0
    assert c[positive_file_path]['X'][0]==[3]
    assert c[negative_file_path]['X'][0]==[2]
    assert len(c[negative_file_path]['X']) == len(c[positive_file_path]['X'])


def test_split_data():
    c = {
    "corpus1":
        {"X":["He is big", "I love being wet","I really love him"],"Y":[1,2,3]},
    "corpus2":{"X":["I have a dog"],"Y":[4]}
    }
    train, test = corpus_management.split_data(c)
    assert len(train["X"]) == 3
    assert len(test["X"]) == 1
