from features import FeatureSexyParentFun
from preprocessing import erotica_managment
import spacy
nlp = spacy.load('en_core_web_sm')

erotica_file_path = 'src/tests/files/erotica_test.txt'
erotica_frecuencydict = erotica_managment.pos_frecuency(['VERB','ADJ','NOUN'],erotica_file_path)

def test_featurize_sexiness():
    feature = FeatureSexyParentFun(erotica_frecuencydict)()
    sentence = "Julia eat it"
    sentence_taged = nlp(sentence)
    features_list = feature.featurize(sentence,{"sentence_taged":sentence_taged})
    assert features_list[0]==1

    sentence = "Julia eat it and it is wet"
    sentence_taged = nlp(sentence)
    features_list = feature.featurize(sentence,{"sentence_taged":sentence_taged})
    assert features_list[0]==2
