from preprocessing import Cleaner

def test_replace_html_entities():
    assert Cleaner.replace_html_entities(Cleaner, "&lt;") == "<"
    assert Cleaner.replace_html_entities(Cleaner, "&gt;") == ">"
    assert Cleaner.replace_html_entities(Cleaner, "&amp;") == "&"
    assert Cleaner.replace_html_entities(Cleaner, "&quot;") == "\""
    assert Cleaner.replace_html_entities(Cleaner, "&apos;") == "'"
    assert Cleaner.replace_html_entities(Cleaner, "&cent;") == "¢"
    assert Cleaner.replace_html_entities(Cleaner, "&pound;") == "£"
    assert Cleaner.replace_html_entities(Cleaner, "&yen;") == "¥"
    assert Cleaner.replace_html_entities(Cleaner, "&euro;") == "€"
    assert Cleaner.replace_html_entities(Cleaner, "&copy;") == "©"
    assert Cleaner.replace_html_entities(Cleaner, "&reg;") == "®"

def test_remove_urls():
    assert Cleaner.remove_urls(Cleaner, "The website https://stackoverflow.com/questions/21209024/python-regex-remove") == "The website"

def test_remove_emojis():
    assert Cleaner.remove_emojis(Cleaner, "This 🐻 is how Lukas typically uses emojis 🐸✌🏼") == "This  is how Lukas typically uses emojis"

def test_split_by_smileys():
    test_sentence = "Sometimes; I feel good: like very-very good :) I also feel sad sometimes :-( or like laughing :D xp"
    res_sentences = ["Sometimes; I feel good: like very-very good", "I also feel sad sometimes", "or like laughing"]
    assert Cleaner.split_by_smileys(Cleaner, test_sentence) == res_sentences

def test_remove_hashtags():
    assert Cleaner.remove_hastags(Cleaner, "Writing assert statements is #boring") == "Writing assert statements is"

def test_strip():
    assert Cleaner.strip(Cleaner, "\n  Removes white spaces and more \t") == "Removes white spaces and more"

def test_normalise_quotationmarks():
    assert Cleaner.normalise_quotationmarks(Cleaner, '“ or ”? We will agree on ".') == '" or "? We will agree on ".'

def test_remove_numbers():
    assert Cleaner.remove_numbers(Cleaner, "W24i4th11984out n0um5b3958ers3 thi8s9 86w3ou485ld769 123b5e343 rea4d32ab3344le.") == "Without numbers this would be readable."

def test_remove_special_chars():
    assert Cleaner.remove_special_chars(Cleaner, "Hey there, I'm #overly /excited/ to %see% *you*!") == "Hey there, I'm overly excited to see you!"

def test_remove_duplicate_punct():
    assert Cleaner.remove_duplicate_punct(Cleaner, "....Whoah!!.!") == ".Whoah!.!"

def test_remove_in_text_line_breaks():
    assert Cleaner.remove_in_text_line_breaks(Cleaner, "The only remaining li\nne breaks\n should be at the end\n! \n\n") == "The only remaining li ne breaks  should be at the end ! \n\n"

def test_remove_tabs():
    assert Cleaner.remove_tabs(Cleaner, "\tno tab\tto be ha\td") == " no tab to be ha d"

def test_remove_excess_whitespace():
    assert Cleaner.remove_excess_white_space(Cleaner, "    Who    goes there?   ") == " Who goes there? "

def test_split_into_sentences():
    test_string = "This is great ! I am fit for this.test. Woah!"
    res_sentences = ['This is great !', 'I am fit for this.test.', 'Woah!']
    assert Cleaner.split_into_sentences(Cleaner, test_string) == res_sentences

def test_replace_slang():
    assert Cleaner.replace_slang(Cleaner, "sry I can't cu tmr b4 ur bday") == "sorry I can't see you tomorrow before your birthday"

def test_filter_by_sentence_length():
    test_sentences = ["This is great!","I am fit for this test.","Woah!"]
    res_sentences = ["This is great!","I am fit for this test."]
    assert Cleaner.filter_by_sentence_length(Cleaner, test_sentences) == res_sentences

def test_return_all_sentences():
    test_sentences = ["This is great. I am a sentence!", "Me too!", "Will you shut up?! Typing to yourself is not funny."]
    res_sentences = ["This is great.", "I am a sentence!", "Me too!", "Will you shut up?!", "Typing to yourself is not funny."]
    assert Cleaner.return_all_sentences(Cleaner, test_sentences) == res_sentences

def test_pipeline():
    test_sentence = " That's i34s a ve%&ry “DIRTY” ;) ]  sent=}ence but idm thx tO the pipe==3line!!!! #lifegoals  "
    res_sentence = "that's is a very \"dirty\" sentence but it doesn't matter thanks to the pipeline!"
    assert Cleaner.clean_pipeline(Cleaner, test_sentence) == res_sentence

def test_clean_and_filter():
    test_sentences = [" That's An unclea$%ne00d sentence... ", "AnOT65her695,,  lon&ger 54unCLEANed sen332t5en/ce.", "Too     short390whe%&ncLeAned"]
    res_sentences = ["that's an uncleaned sentence.", "another, longer uncleaned sentence."]
    assert Cleaner.clean_and_filter(Cleaner, test_sentences)[0] in res_sentences
    assert Cleaner.clean_and_filter(Cleaner, test_sentences)[1] in res_sentences

def test_basic_clean_and_split():
    test_sentences = ["That&apos;s an example of a string that is split by a smiley :) You get it?", "That&apos;s just a normal sentence", "This sentence is normal too"]
    res_sentences = ["That's an example of a string that is split by a smiley", "You get it?", "That's just a normal sentence", "This sentence is normal too"]
    assert Cleaner.basic_clean_and_split(Cleaner, test_sentences) == res_sentences
