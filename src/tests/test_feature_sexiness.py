from features import FeatureSexinessFun
from preprocessing import erotica_managment
import spacy
nlp = spacy.load('en_core_web_sm')

erotica_file_path = 'src/tests/files/erotica_test.txt'
erotica_frecuencydict = erotica_managment.pos_frecuency(['VERB','ADJ','NOUN'],erotica_file_path)

def test_featurize_sexiness():
    feature = FeatureSexinessFun(erotica_frecuencydict)()
    assert feature.frecuencies['VERB']['eat']==1
    sentence = "Lukas eat your salad"
    sentence_taged = nlp(sentence)
    features_list = feature.featurize(sentence,{"sentence_taged":sentence_taged})
    assert features_list[0]==1
    assert features_list[1]==0
    assert features_list[2]==0

def test_featurize_sexiness():
    feature = FeatureSexinessFun(erotica_frecuencydict)()
    assert feature.frecuencies['VERB']['eat']==1
    sentence = "Lukas eat your dick with fork"
    sentence_taged = nlp(sentence)
    features_list = feature.featurize(sentence,{"sentence_taged":sentence_taged})
    finfo = feature.get_info(features_list)
    print(erotica_frecuencydict)
    assert finfo['VERB_frecuency_ratio']==1
    assert finfo['ADJ_frecuency_ratio']==0
    assert finfo['NOUN_frecuency_ratio']==0 #.5 rounded
