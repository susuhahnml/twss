from features import FeaturePronounNgramsFun


def test_pronouns_get_info():
    vectorizer = FeaturePronounNgramsFun(['it', 'me', 'them', 'you'])() #second bracets create the object of the class, first pass to the function
    list = vectorizer.featurize('hello knock knock it')
    assert list[0]==1
    assert list[1]==0
    assert list[2]==0
    assert list[3]==0

    list2 = vectorizer.featurize('is it me or is it you')
    info_dict = vectorizer.get_info(list2)
    assert info_dict["it"] ==2
    assert info_dict["me"] ==1
    assert info_dict["you"] ==1
    assert not "them" in info_dict
