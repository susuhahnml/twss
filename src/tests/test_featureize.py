from features import *
from preprocessing import erotica_managment

data = ['This is big','Can I lick that?']
erotica_test_path = 'src/tests/files/erotica_test.txt'
erotica_frecuencydict = erotica_managment.pos_frecuency(['VERB','ADJ','NOUN'],erotica_test_path)

def test_featurize():
    featurizer = Featurizer.create_form_classes([FeatureWordsNumber])
    features_list = featurizer.featurize("This is a test function")
    assert len(features_list) ==1
    assert features_list[0]==5

def test_get_all_features_info():
    featurizer = Featurizer.create_form_classes([FeatureWordsNumber])
    info = featurizer.get_all_features_info([12])
    assert "FeatureWordsNumber" in info

def test_pretty_print():
    featurizer = Featurizer.create_form_classes([FeatureWordsNumber])
    featurizer.pretty_print([12])

def test_feature_info():
    featurizer = Featurizer.create_form_classes([FeatureWordsNumber])
    info = featurizer.get_one_feature_info([12], FeatureWordsNumber)
    assert "number_of_words" in info
    assert info["number_of_words"] == 12

def test_featurize_all():
    feature_classes = [
    FeatureWordsNumber,
    FeatureCharacterNgramsFun(['hello need are you', 'I am eating', 'functions are good for test','This is good']),
    FeaturePronounNgramsFun(),
    FeatureSexinessFun(erotica_frecuencydict),
    FeatureUnsexyNounsNumberFun(erotica_frecuencydict),
    FeatureTriggeringWordsFun(erotica_frecuencydict)
    ]
    featurizer = Featurizer.create_form_classes(feature_classes)
    features_list = featurizer.featurize("I need you to eat this")
    featurizer.pretty_print(features_list)
    info = featurizer.get_all_features_info(features_list)
