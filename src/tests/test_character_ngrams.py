from features import FeatureCharacterNgramsFun

def test_ngrams_get_info():
    feature = FeatureCharacterNgramsFun(['hello how are you', 'good and you'])() #second bracets create the object of the class, first pass to the function
    list = feature.featurize('hello you you you')
    info = feature.get_info(list)
    assert info[" you"]==3
    assert info["hell"]==1
