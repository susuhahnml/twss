from classifier import *
from preprocessing import corpus_management
from features import *
from sklearn.exceptions import UndefinedMetricWarning
import warnings
warnings.filterwarnings(action='ignore', category=UndefinedMetricWarning)


positive_files = ['../twss/src/tests/files/positive_test.txt']
negative_files = ['../twss/src/tests/files/negative_test.txt']
pronounlist = ['I', 'you', 'he', 'she', 'it', 'we']
corpus_list = corpus_management.combine_corpora(positive_files+negative_files)
feature_classes = [
FeatureWordsNumber,
FeatureCharacterNgramsFun(corpus_list),
FeaturePronounNgramsFun(pronounlist = pronounlist)
]
featurizer = Featurizer.create_form_classes(feature_classes)
featurized_corpus_dict = corpus_management.featurize_corpus(positive_files, negative_files, featurizer, True)
train, test = corpus_management.split_data(featurized_corpus_dict)

def test_make_classifier_multinomial():
    classifierMB = ClassifierMultinomialBayes.init_train(featurizer= featurizer, corpus= train) # cleaner = cleaner,
    predicted_y = classifierMB.predict(test['X'], featurized = True)
    classifierMB.show_best_parameters()
    evaluate_score = classifierMB.evaluate(list(test['Y']), predicted_y)
    print(evaluate_score)
    weights = classifierMB.get_weights()
    print(weights)
    twss= classifierMB.predict("You big", featurized=False)
    assert twss[0]==1
    twss= classifierMB.predict("I love my mom", featurized=False)


def test_make_classifier_logistic_regression():
    classifierLR = ClassifierLogisticRegression.init_train(featurizer= featurizer, corpus= train) # cleaner = cleaner,
    predicted_y = classifierLR.predict(test['X'], featurized = True)
    classifierLR.show_best_parameters()
    evaluate_score = classifierLR.evaluate(list(test['Y']), predicted_y)
    print(evaluate_score)
    weights = classifierLR.get_weights()
    twss= classifierLR.predict("You big", featurized=False)
    assert twss[0]==1

def test_file_prob():
    classifierLR = ClassifierLogisticRegression.init_train(featurizer= featurizer, corpus= train) # cleaner = cleaner,
    twss= classifierLR.predict_to_file(negative_files[0])
