import numpy as np
from collections import defaultdict
from sklearn.model_selection import train_test_split
from preprocessing import erotica_managment
import os
import random
import sys

def featurize_corpus(positive_files,negative_files,featurizer, force_re_train=False, balance_corpora=True):
    """
    Reads the positive and negative files. Creates a dictionary with the featurized
    sentences.
    Args:
        positive_files: Array of file paths with positive sentences.
        negative_files: Array of file paths with negative sentences.
        featurizer: Used to transform sentence to features
        force_re_train: Featurizes corpus even if a saved instance exists already. Default: false
        balance_corpora: Balances number of samples in negative and positive training set. Default: true
    Returns:
        A dictionary with file names as keys and dictionaries as values
        containing X and Y
    """
    path = 'corpus/featurized_dict.json'
    has_saved = os.path.exists(path)
    if(has_saved and not force_re_train):
        print("Recovering saved corpus featurized...")
        return erotica_managment.read_json_file(path)

    print("Featurizing corpus...")

    corpus = {}
    total_negative_size = 0
    positive_size = 0
    negative_size = 0

    for filename in positive_files:
        text = [line.rstrip('\n').replace(u'\xa0', ' ') for line in open(filename)]
        positive_size+=len(text)
        corpus[filename] = {'X': [featurizer.featurize(s) for s in text],'Y': [1]*len(text)}

    if balance_corpora:
        #get texts and size counts of negative sub-corpora
        percentages = defaultdict()
        texts = defaultdict()
        for filename in negative_files:
            text = [line.rstrip('\n').replace(u'\xa0', ' ') for line in open(filename)]
            total_negative_size+=len(text)
            percentages[filename] = len(text)
            texts[filename] = text
        #compute percentages
        for key in percentages.keys():
            percentages[key] = percentages[key]/total_negative_size
        #compose negative corpora at random based on percentages
        for filename in negative_files:
            number_to_add = round(percentages[filename]*positive_size)
            selected_texts = texts[filename][len(texts[filename])-number_to_add:]
            negative_size+=len(selected_texts)
            corpus[filename] = {'X': [featurizer.featurize(s) for s in selected_texts],'Y': [0]*len(selected_texts)}
    else:
        for filename in negative_files:
            text = [line.rstrip('\n').replace(u'\xa0', ' ') for line in open(filename)]
            negative_size+=len(text)
            corpus[filename] = {'X': [featurizer.featurize(s) for s in text],'Y': [0]*len(text)}

    print("Working with ", positive_size, " positive sentences and ", negative_size, " negative senteces")
    return corpus



def combine_corpora(filenames_list):
    """
    This will combine all corpora to one big list containing all the sentences
    for the fitting of the ngram count vectorizer
    Args:
        List of filenames
    Retruns:
        List of strings
    """
    corpora = []
    for filename in filenames_list:
        corpora.extend([line.rstrip('\n').replace(u'\xa0', ' ') for line in open(filename)])
    return corpora


def split_data(dict_corpus):
    """
    Joins all corpus on the dictionary.
    Splits the dictionary with the corpus into train and test
    Args:
        A dictionary with file names as keys and dictionaries as values
        containing X and Y
    Returns:
        A tuple of two dictionaries for (train_data, test_data)
    """
    print("Spliting corpus...")

    X = []
    Y = []
    for data in dict_corpus.values():
        X.extend(data['X'])
        Y.extend(data['Y'])

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.20, random_state=42)

    traindict = {}
    traindict['X'] = X_train
    traindict['Y'] = y_train

    testdict = {}
    testdict['X'] = X_test
    testdict['Y'] = y_test

    return (traindict, testdict)
