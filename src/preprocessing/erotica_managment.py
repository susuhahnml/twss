import json
from spacy.lemmatizer import Lemmatizer
from spacy.lang.en import LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES
from collections import defaultdict
import spacy
nlp = spacy.load('en_core_web_sm')
import os
def pos_frecuency(postags, filename):
    """
    Gets frecuencies of words that are part of the defined pos tags
    Args:
        postags: Array of universal postags to use
        filename: The file to get the frecuency from. Must be cleaned
    Returns:
        A dictionary with the postags as keys and the dictionary of frecuency as value
    """
    frecuencies = {}
    for tag in postags:
        frecuencies[tag] = defaultdict(int)
    file = open(filename,'r')
    for sentence in file:
        sentence_taged = nlp(sentence)
        for token in sentence_taged:
            if(not token.pos_ in postags):
                continue
            frecuencies[token.pos_][token.text]+=1
    return {pos:dict(d) for pos, d in frecuencies.items()}

def write_json_file(path, data):
    """
    Writes a dictionary into a json file
    Args:
        path: The path of the output file
        data: The dicitionary to write
    """
    with open(path, 'w') as outfile:
        json.dump(data, outfile)

def read_json_file(path):
    """
    Writes a dictionary into a json file
    Args:
        path: The path of the input file
    """
    with open(path) as infile:
        data = json.load(infile)
    return data

def process_sentences_to_json_file(erotica_file_path, out):
    """
    Reads a file postags it and saves the dictionary in a file
    Args:
        erotica_file_path: The path of the input file of sentences
        out: The path of the output file
    """
    frecuencydict = pos_frecuency(['VERB','ADJ','NOUN','PRON'],erotica_file_path)
    import json
    with open(out, 'w') as outfile:
        json.dump(dict(frecuencydict), outfile)


def set_minimum_frecuency(minimum):
    """
    Removes all frecuencies lower than minimum
    """
    def p(erotica_dict):
        new = {}
        for pos, frecuencies in erotica_dict.items():
            new[pos]={w:f for w, f in frecuencies.items() if f>minimum}
        return new
    return p

def divide_frecuencies(minimum):
    """
    Divides all frecuencies by minimum and saves an integer
    """
    def p(erotica_dict):
        new = {}
        for pos, frecuencies in erotica_dict.items():
            new[pos]={w:int(f/minimum) for w, f in frecuencies.items()}
        return new
    return p

def lematize(erotica_dict):
    """
    Lematizes and adds corresponing ocurences
    Args:
        erotica_dict: The dictionary with the erotica words
    """
    lemmatizer = Lemmatizer(LEMMA_INDEX, LEMMA_EXC, LEMMA_RULES)
    dict_lematized = {}
    for pos, frecuencies in erotica_dict.items():
        dict_lematized[pos]={}
        for word, f in frecuencies.items():
            lemma = lemmatizer(word,pos)[0]
            if lemma in dict_lematized[pos]:
                dict_lematized[pos][lemma]+=f
            else:
                dict_lematized[pos][lemma]=f
    return dict_lematized

def sort_dict_erotica(erotica_dict):
    """
    Sorts by frecuency value
    Args:
        erotica_dict: The dictionary with the erotica words
    """
    sorted_dict = {}
    for pos, frecuencies in erotica_dict.items():
        sorted_items=sorted(frecuencies.items(), key=(lambda kv : (kv[1],kv[0])))
        sorted_dict[pos]={k:v for (k,v) in sorted_items[::-1]}
    return sorted_dict
