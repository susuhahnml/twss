from erotica_managment import *
from functools import reduce

"""
Script to process all erotica words
"""

in_file = "../../corpus/erotica_frecuencies_original.json"
out_file = "../../corpus/erotica_frecuencies_processed.json"

dict = read_json_file(in_file)
functions = [read_json_file,
            set_minimum_frecuency(5),
            lematize,
            set_minimum_frecuency(2000),
            divide_frecuencies(2000),
            sort_dict_erotica]

pipeline = reduce(lambda f, g: lambda x: f(g(x)), functions[::-1], lambda x: x)
dict_new = pipeline(in_file)
write_json_file(out_file,dict_new)
