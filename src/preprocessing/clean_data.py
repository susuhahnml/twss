from cleaner import Cleaner as cl

"""
Script to clean the corpus data
"""

#CLEANING AND MERGING TWSS PHRASES
#getting all texts from the twssstories_data.csv and saving cleaned list of TWSS-triggering sentences merged with orignal corpus as positive corpus
in_file = "raw_data/twssstories_data.csv"
cl.load_clean_save(cl, in_file, input_type="twss")

#CLEANING EROTICA
in_file = "raw_data/erotica.csv"
cl.load_clean_save(cl, in_file, input_type="erotica")

#CLEANING PHRASES AND SAYINGS
in_file = 'raw_data/phrases_sayings.csv'
cl.load_clean_save(cl, in_file, input_type="sayings")

#CLEANING SUBTITILES
filenames = [
"unannotated/sausage_party.txt",
"unannotated/cinderella.txt",
"unannotated/lion_king.txt"]
for filename in filenames:
	cl.load_clean_save(cl, filename, input_type='subtitles')

#CLEANING MANUALLY PRE-CLEANED CORPORA
filenames =  [
'positive/common_phrases.txt',
'positive/urbandictionary.txt',
'negative/bodyphrases.txt',
'negative/common_phrases.txt',
'negative/movieconversations.txt',
'negative/SMSSpamCollection.txt']
for filename in filenames:
	cl.load_clean_save(cl, filename)
