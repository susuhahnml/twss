from scraper import Scraper as sc

"""
Script to scrape all data from web
"""

#scraping https://theoffice.fandom.com/wiki/List_of_the_times_somebody_says_%22That's_what_she_said%22 for all phrases from the office
base_url = "https://theoffice.fandom.com/wiki/List_of_the_times_somebody_says_%22That's_what_she_said%22"
req_dict = {1: {'row-tag': 'tr', 'col-tag': 'td', 'col-ids': ['ep_num', 'ep_name','speaker','text']}}
file = "../../corpus/raw_data/office_lines.csv"
raw_office_lines = sc.scrape_normal_table(sc, base_url, req_dict, print_process=True)
sc.write_raw_csv(sc, raw_office_lines, file)

#scraping www.urbandictionary.com
base_url = "https://www.urbandictionary.com/define.php?term=that%27s%20what%20she%20said"
req_dict = {1: {'type': 'div', 'name': 'example', 'output': 'text', 'sub_elem': None, 'to_int':False},
            2: {'type': 'a', 'name': 'up', 'output': 'likes', 'sub_elem': {'type': 'span', 'name': 'count'}, 'to_int':True},
            3: {'type': 'a', 'name': 'down', 'output': 'dislikes', 'sub_elem': {'type': 'span', 'name': 'count'}, 'to_int':True}}
file = "../../corpus/raw_data/urban_dict_data.csv"
raw_urbndct = sc.scrape_page(sc, base_url, 0, 16, req_dict, connector="&page=", print_process=True) #15 as maximum page count -> range 0..16
sc.write_raw_csv(sc, raw_urbndct, file)

#scraping www.phrases.org.uk for phrases and sayings
base_url = "https://www.phrases.org.uk/meanings/phrases-and-sayings-list.html"
req_dict = {1: {'type': 'p', 'name': 'phrase-list', 'output': 'text', 'sub_elem': None, 'to_int':False}}
file = "../../corpus/raw_data/phrases_sayings.csv"
raw_sayings = sc.scrape_page(sc, base_url, 0, 1, req_dict, print_process=True) #only one page
sc.write_raw_csv(sc, raw_sayings, file)

#scraping www.phrases.org.uk for phrases and sayings
base_url = "https://www.phrases.org.uk/meanings/body.html"
req_dict = {1: {'type': 'p', 'name': 'meanings-body', 'output': 'text', 'sub_elem': None, 'to_int':False}}
file = "../../corpus/raw_data/phrases_body.csv"
raw_body = sc.scrape_page(sc, base_url, 0, 1, req_dict, print_process=True) #only one page
sc.write_raw_csv(sc, raw_body, file)

#scraping textfiles.com/sex/EROTICA for all possible erotic texts
base_url = "http://textfiles.com/sex/EROTICA/"
req_dict1 = {1: {'row-tag': 'tr', 'col-tag': 'b', 'col-ids': ['letter', 'desc']},
            2: {'row-tag': 'tr', 'col-tag': None, 'col-ids': ['filename', 'size', 'desc']}}
req_dict2 = {1: {'type': 'all', 'output': 'text', 'sub_elem': None, 'to_int':False}}
file = "../../corpus/raw_data/erotica.csv"
data_alphabetic = sc.scrape_link_info(sc, base_url, req_dict1, 1, print_process=True)
raw_erotica = sc.get_all_txt_files(sc, base_url, req_dict2, data_alphabetic, print_process=True)
sc.write_raw_csv(sc, raw_erotica, file)

#scraping www.twssstories.com from the wayback machine
base_url = "https://web.archive.org/web/20160827164700/http://www.twssstories.com:80/"
req_dict = {1: {'type': 'div', 'name': 'content clear-block', 'output': 'text', 'sub_elem': None, 'to_int':False},
            2: {'type': 'span', 'name': 'total-votes', 'output': 'laughs', 'sub_elem': None, 'to_int':True}}
file = "../../corpus/raw_data/twssstories_data.csv"
raw_twssstrs = sc.scrape_page(sc, base_url, 0, 230, req_dict, connector="node?page=", print_process=True) #229 as maximum page count/last accessible page --> range 0..230
sc.write_raw_csv(sc, raw_twssstrs, file)
