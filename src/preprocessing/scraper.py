import os #for checking and creating directories in "write_raw_csv" function
import csv #for writing csv in "write_raw_csv" function
import urllib.request #for requesting webpages in "request_page" function
from bs4 import BeautifulSoup #for parsing and searching html in "request_page" and "return_element_contents" functions

import sys   
sys.setrecursionlimit(10000) #have to increase recursion limit to avoid bug in BeautifulSauce

"""
Class containing all relevant scraping functions
"""
class Scraper(object):

    def __init__(self):
        pass
####GENERAL FRAMWORK FOR SCRAPING OBJECTS OF INDIVIDUAL WEB PAGES

    def make_url(self, base_url, connector, index):
        """
        Returns complete url string for given base_url, connector and index. 
        If index is 0, only the base url will be returned.

        Args:
            base_url (str): Url representing the basic web page to be scraped for.
            connector (str): String specifying the way the next page is represented in the url.
            index (int): Number indicating page index .

        Returns:
            str: Complete string for given base_url, connector and index.
        """
        if index == 0:
            url = base_url
        else:
            url = base_url + connector + str(index)
        return url

    def request_page(self, url):
        """
        Sends request to given url and returns a BeautifulSoup object with the parsed html.

        Args:
            url (str): The url to be scraped.

        Returns:
            BeautifulSoup: BeautifulSoup object with parsed html of the url.
        """
        request = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
        response = urllib.request.urlopen(request)
        soup = BeautifulSoup(response, 'html.parser')
        return soup

    def return_element_contents(self, html_soup, class_dict):
        """
        Returns the content of the elements specified in the class_dict object from a given HTML-BeautifulSoup object.

        Args:
            html_soup (BeautifulSoup): BeautifulSoup object containing a parsed html file.
            class_dict (dict): Python dictionary specifying which elements are to be scraped from the html.

        Returns:
            list: List of dictionaries containing the specified elements grouped by index of occurence.
        """
        res_dict = {}
        for key, vals in class_dict.items(): #for each element specified in dictionary
            container_list = [html_soup] #default (for input 'all') is just the soup
            if vals['type']!='all': #if the type is specified further, rerieves all elemets of type and name as specified
                container_list = html_soup.find_all(vals['type'], attrs={'class': vals['name']})
            for index, elem in enumerate(container_list): # for each element in the resulting lists
                #reduces elements to sub-element if specified
                if vals['sub_elem'] != None:
                    subvals = vals['sub_elem']
                    container_list[index] = container_list[index].find(subvals['type'], attrs={'class': subvals['name']})
                #removes text between < > and converts to int if specified
                container_list[index] = self.remove_text_between(self, container_list[index], '<', '>', vals['to_int'])
                if index not in res_dict:
                    res_dict[index] = {}
                #saves content of element to dictionary key for index
                res_dict[index][vals['output']] = container_list[index]        
        res_list = list(res_dict.values())
        return res_list  

    def remove_text_between(self, text, starting_char, ending_char, to_int=False):
        """
        Returns a given string with all characters between and including the given starting_char and ending_char removed.
        Can also return an integer if specified.

        Args:
            text (str): text to be reduced.
            starting_char (str): starting character.
            ending_char (str): ending character.
            to_int (bool): default=False, returns int if set to True.

        Returns:
            str: The reduced string, only containing characters NOT between starting and ending characters.
        """
        split_text = list(str(text))
        starting_indices = [i for i, char in enumerate(split_text) if char == starting_char]
        ending_indices = [i+1 for i, char in enumerate(split_text) if char == ending_char]
        deletion_counter = 0
        for i, index in enumerate(starting_indices):
            del split_text[index-deletion_counter:ending_indices[i]-deletion_counter]
            deletion_counter += (ending_indices[i]-index)
        if to_int == True:
            return int(''.join([char for char in split_text if char.isdigit()]))
        else:
            res_string = ''.join(split_text)
            return res_string
        
    def write_raw_csv(self, res_dict, filename):
        """
        Takes a dictionary and writes it with header as csv to a specified location. If the specified location does not
        the function will try and create it.

        Args:
            res_dict (dict): The dictionary to be written to the file.
            filename (str): String describing file location.
        """
        #creates file path directory by directory if non-existent
        sub_dirs = filename.split("/")#
        current_dir = sub_dirs[0]#
        for i in range(0, len(sub_dirs)-1):#
            if not os.path.exists(current_dir):
                os.makedirs(current_dir)
            current_dir = current_dir+"/"+sub_dirs[i+1]

        #writes dict to file path
        with open(filename, 'w') as file:
            fieldnames = list(list(res_dict[1].keys()))
            wr = csv.DictWriter(file, fieldnames=fieldnames,delimiter=chr(255))
            wr.writeheader()
            for key, vals in res_dict.items():
                if vals:
                    wr.writerow(vals)

    def scrape_page(self, base_url, low_bound, up_bound, elem_dict, connector="", print_process=False):
        """
        Saves the content of specified elements in a dictionary for a base url and a specified number of pages
        connected by a connector.

        Args:
        base_url (str): Basic url mostly representing the first page from which should be scraped.
        low_bound (int): Parameter representing the starting index from which shall be scraped (0: from base_url).
        up_bound (int): Parameter representing the stopping index to which shall be scraped.
        elem_dict (dict): Python dictionary specifying which elements are to be scraped from the html.
        connector (str): Parameter specifying the way the pages are attached to the base_url.
        print_process (bool): Boolean specifying if progress updates are printed to the console during scraping 
                              (default: False).

        Returns:
        dict: Dictionary with keys for the indices iterated over and sub-dictionaries for the elements specified in 
              the elem_dict.
        """
        overall_index = 0
        res_dict = {}
        for index in range (low_bound, up_bound):
            url = self.make_url(self, base_url, connector, index)
            try:
                page_soup = self.request_page(self, url)
                content = self.return_element_contents(self, page_soup, elem_dict)
                for element in content:
                    res_dict[overall_index] = element
                    overall_index += 1
                if print_process==True:
                    print("page {} parsed".format(url))
            except:
                if print_process==True:
                    print("page {} could not be parsed".format(url))             
        return res_dict

####ADDITIONAL FUNCTIONS FOR SCRAPING URL-INFO FROM A TABLE AND RETRIEVING ELEMENTS FROM URL
    def scrape_link_info(self, url, elem_dict, starting_id, print_process=False):
        """
        Scrapes url for elements provided in elem_dict. Takes the content for the element's dictionary key "letter" to
        construct a new url for which it returns the elements specified in the second entry of the elem_dict. 
        The resulting dictionary details all possible text files for each extended url.

        Args:
            url (str): The url to be scraped.
            elem_dict (dict): Python dictionary specifying which elements are to be scraped from the html.
            starting_id (int): Integer, specifying the key in elem_dict from which to start.
            print_process (bool): Boolean specifying if progress updates are printed to the console during scraping 
                                    (default: False).

        Returns:
            dict: Dictionary with keys for all first level letters and a list of sub-dictionaries with all possible texts 
                    info in each sub-url.
        """
        level_dict = {}
        page_soup = self.request_page(self, url)
        top_level_content = self.return_table_contents(self, page_soup, elem_dict[starting_id])
        for element in top_level_content:
            new_url = url+element['letter']
            page_soup = self.request_page(self, new_url)
            low_level_content = self.return_table_contents(self, page_soup, elem_dict[starting_id+1])
            level_dict[element['letter']] = low_level_content
            if print_process==True: 
                print("Saved all info for Erotica starting with {}.".format(element['letter']))
        return level_dict

    def return_table_contents(self, html_soup, class_vals):
        """
        Returns the content of the elements specified in the class_vals dictionary from a given HTML-BeautifulSoup object.

        Args:
            html_soup (BeautifulSoup): BeautifulSoup object containing a parsed html file.
            class_vals (dict): Python dictionary specifying which elements are to be scraped from the html.

        Returns:
            list: List of dictionaries containing the specified elements grouped by index of occurence.
        """
        res_dict = {}
        container_list = html_soup.findAll(class_vals['row-tag']) #initiate the container_list with all 'row-tag' elements per default
        if class_vals['col-tag']==None:
            # if 'col-tag' not defined, take first instance of container_list (likely to hold all neccessary info) and create container_list from it
            container_list = self.create_container_list_from_string(self, container_list[0])
        for index, elem in enumerate(container_list): # for each element in the container_list
            if class_vals['col-tag']!=None: #if container_list hs not yet been manually split through create_container_list_from_string
                container_list[index] = container_list[index].findAll(class_vals['col-tag']) #find all sub-tags for columns 
            if index not in res_dict:
                res_dict[index] = {}
            for i, col_id in enumerate(class_vals['col-ids']): #save the sub-elements in dictionary for entry name specified in col-id
                if container_list[index] != []:
                    if class_vals['col-tag']!=None: 
                        res_dict[index][col_id] = self.remove_text_between(self, container_list[index][i], '<', '>', False)
                    else:
                        res_dict[index][col_id] = container_list[index][i]
        res_list = list(res_dict.values())
        return res_list   

    def create_container_list_from_string(self, string):
        """
        To be used if container_list contains all elements for all rows instead of all elements for one row.
        Takes a string and converts it into an appropriate format for the return_table_contents function.

        Args:
            string (str): The string to be converted.

        Returns:
            list: List of lists with each sublist representing the column vaues for each row.
        """
        row_string = self.remove_text_between(self, string, '<', '>', False) #removes html tags
        row_string = row_string.split("\n") #splits in rows
        row_string = [elem.split() for elem in row_string] #splits into individual elements
        row_string = [[elem[0], elem[1],' '.join(elem[2:])] for elem in row_string if len(elem)>2] #makes 3 out of x elements
        return row_string

    def get_all_txt_files(self, url, elem_dict, info_dict, print_process=False):
        """
        Tries to create a url for all sub_values in an info_dict and parse the content of said url.

        Args:
            url (str): The url to be scraped.
            elem_dict (dict): Python dictionary specifying which elements are to be scraped from the html.
            info_dict (dict): Python dictionary holding all strings to be connected together for url to point to text.
            print_process (bool): Boolean specifying if progress updates are printed to the console during scraping 
                                    (default: False).

        Returns:
            dict: Dictionary with keys for the indices iterated over and sub-dictionaries holding "filename", "text" and
                    description of element specified in elem_dict.
        """
        final_dict = {}
        overall_index = 0
        for key1 in list(info_dict.keys()):
            dict_list = info_dict[key1]
            for dct in dict_list:
                key2 = dct['filename']
                key2 = key2.replace('&amp;','&')
                description = dct['desc']
                goal_url = url + key1 + "/" + key2
                try:
                    content = self.scrape_page(self, goal_url, 0, 1, elem_dict, print_process=print_process)
                    final_dict[overall_index] = {'filename':key2, 'text':content[0]['text'], 'desc':description}
                    overall_index += 1
                except:
                    if print_process==True:
                        print("{} could not be saved to database.".format(key2))
        return final_dict

    def scrape_normal_table(self, url, elem_dict, print_process=False):
        """
        Scrapes a table at the given url. Columns are scaped sequentially so col-ids in elem dict should be specified until
        until data point of interest.

        Args:
            url (str): The url to be scraped.
            elem_dict (dict): Python dictionary specifying which elements are to be scraped from the html.
            print_process (bool): Boolean specifying if progress updates are printed to the console during scraping 
                                    (default: False).

        Returns:
            dict: Dictionary with keys for the rows iterated over and sub-dictionaries holding the specified columns.
        """
        final_dict = {}
        page_soup = self.request_page(self, url)
        try:
            data = self.return_table_contents(self, page_soup, elem_dict[1])
            for index, sub_dict in enumerate(data):
                final_dict[index] = sub_dict
            if print_process == True:
                print("page {} parsed.".format(url))
        except:
            if print_process == True:
                print("page {} could not be parsed.".format(url))
        return final_dict