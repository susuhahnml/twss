from functools import reduce
import re
import os
import csv
import sys
from io import StringIO
import html
import string
csv.field_size_limit(sys.maxsize) #have to increase field size limit in order to be able to read in all csv data


"""
Class containing all relevant cleaning functions
"""
class Cleaner(object):

    def __init__(self):
        pass

    def load_text_from_csv(self, filepath, column_name):
        """
        Loads all data from a specified column in a .csv file into a list and returns that list.

        Args:
            filepath (str): String describing location of csv.
            column_name (str): String specifying description name the column to read from.

        Returns:
            list: List with an element for each field in the specified column.
        """
        all_texts = []
        with open(filepath, 'rt') as f:
            data = f.read()
            data = data.replace('\x00','')
            reader = csv.DictReader(StringIO(data), delimiter='ÿ')
            for row in reader:
                all_texts.append(row[column_name])
        return all_texts

    def load_sentences_from_txt(self, filepath):
        """
        Loads all data from each line of a .txt file into a list and returns that list.

        Args:
            filepath (str): String describing location of txt.

        Returns:
            list: List with an element for each line in the .txt-file.
        """
        with open(filepath) as f:
            content = f.readlines()
        return content

    def write_list_to_file(self, input_list, filepath):
        """
        Takes a list and writes it to a specified file location with each element in the list in a new line.
        If the specified location does not exist the function will try and create it.

        Args:
            input_list (list): The list to be written to the file.
            filepath (str): String describing outpit file location.
        """
        #creates file path directory by directory if non-existent
        sub_dirs = filepath.split("/")
        current_dir = sub_dirs[0]
        for i in range(0, len(sub_dirs)-1):
            if not os.path.exists(current_dir):
                os.makedirs(current_dir)
            current_dir = current_dir+"/"+sub_dirs[i+1]

        #writes to file
        with open(filepath, 'w') as f:
            for elem in input_list:
                f.write("%s\n" % elem)

#### OPERATIONS FOR SINGLE STRINGS
    def replace_html_entities(self, text):
        """
        Replaces all html entities in a string with the corresponding character.

        Args:
            text (str): String to be checked for html entities.

        Returns:
            str: String with all html elements replaced.
        """
        no_null = text.replace("\xa0", " ")
        return html.unescape(no_null)

    def remove_urls(self, text):
        """
        Removes all urls from a given input string.

        Args:
            text (str): String to be checked for urls.

        Returns:
            str: String with all urls removed.
        """
        url_reg = re.compile(r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')
        return re.sub(url_reg,r'',text).strip()

    def split_by_smileys(self, text):
        """
        Splits a given input string into a list of strings by smileys.

        Args:
            text (str): String to be split by smileys.

        Returns:
            list: List containing all substrings if split by smileys or just the original string.
        """
        smiley_reg = re.compile(r"( X|:|;|=)(-)?(\)|\(|O|D|P)", re.IGNORECASE)
        split_text = re.split(smiley_reg,text)
        if isinstance(split_text, list):
            truncated = [split.strip() for split in split_text if split]
            return [split for split in truncated if len(split)>1]
        else:
            return [split_text]

    def remove_emojis(self, text):
        """
        Removes all emojis from a given input string.

        Args:
            text (str): String to be checked for emojis.

        Returns:
            str: String with all emojis removed.
        """
        try:
        # UCS-4
            emoji_reg = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
        except re.error:
        # UCS-2
            emoji_reg = re.compile(u'([\u2600-\u27BF])|([\uD83C][\uDF00-\uDFFF])|([\uD83D][\uDC00-\uDE4F])|([\uD83D][\uDE80-\uDEFF])')
        return re.sub(emoji_reg,r'',text).strip()

    def remove_hastags(self, text):
        """
        Removes all hastags from a given input string.

        Args:
            text (str): String to be checked for hashtags.

        Returns:
            str: String with all hashtags removed.
        """
        return re.sub(r'#\w*',r'',text).strip()

    def strip(self, text):
        """
        Removes all default whitespace characters from start and end of a given input string.

        Args:
            text (str): String to be checked for default whitespace characters at start/end.

        Returns:
            str: String with all default whitespace characters at both ends removed.
        """
        return text.strip()

    def normalise_quotationmarks(self, text):
        """
        Replaces starting and ending quotatation marks “ and ” with the standard quotation mark ".

        Args:
            text (str): String to be checked for starting and ending quotatation marks.

        Returns:
            str: String with all starting and ending quotatation marks replaced.
        """
        return re.sub(r'[“”]',r'"',text)

    def remove_special_chars(self, text, excl_chars='\-.,!?\'\"'):
        """
        Removes all special characters - excluding the characters in the excl_chars string - from a given input string.

        Args:
            text (str): String to be checked for special characters.
            excl_chars (str): String of non alphanumeric characters which are not supposed to be removed.
                                The default is standard punctuation (. , ! ? ' ").

        Returns:
            str: String with all special characters removed.
        """
        reg_ex = r'[^\w '+excl_chars+']'
        return re.sub(reg_ex,r'',text)

    def remove_numbers(self, text):
        """
        Removes all numbers from a given input string.

        Args:
            text (str): String to be checked for numbers.

        Returns:
            str: String with all numbers removed.
        """
        return re.sub(r'\d',r'',text)

    def remove_duplicate_punct(self, text):
        """
        Removes all duplicate punctuation from a given input string.

        Args:
            text (str): String to be checked for duplicate punctuation.

        Returns:
            str: String with all duplicate punctuation removed.
        """
        try:
            return ''.join([text[i] if text[i] not in string.punctuation else text[i] if text[i+1]!= text[i] else '' for i in range(len(text)-1)]+[text[-1]])
        except:
            return ''

    def remove_in_text_line_breaks(self, text):
        """
        Removes all in-text line breaks (line breaks with punctuation or a alphabetic character before or after them) from a given input string.

        Args:
            text (str): String to be checked for in-text line breaks.

        Returns:
            str: String with all in-text line breaks removed.
        """
        return re.sub(r'(?<=[\w .!?,])\n(?=[\w .!?,\t\"])',r' ',text)

    def remove_tabs(self, text):
        """
        Removes all tabs (\t) from a given input string.

        Args:
            text (str): String to be checked for tabs.

        Returns:
            str: String with all tabs removed.
        """
        return re.sub(r'\t',r' ',text)

    def remove_excess_white_space(self, text):
        """
        Removes all excess white space from a given input string.

        Args:
            text (str): String to be checked for excess white space.

        Returns:
            str: String with all excess white space removed.
        """
        return re.sub(r' [ ]*',r' ',text)

    def split_into_sentences(self, text):
        return re.split('(?<=[.!?]) +',text)

    def replace_slang(self, text):
        """
        Replaces English slang words in a given input string if they are listed in the slang dictionary with proper English words.

        Args:
            text (str): String to be checked for slang words.

        Returns:
            str: String with all recognised slang words replaced.
        """

        #extracted from http://www.illumasolutions.com/omg-plz-lol-idk-idc-btw-brb-jk.htm
        SLANG_DICT = {'143': 'I love you', '459': 'I love you', '2moro': 'tomorrow', '2nite': 'tonight', '411': 'information',
         '420': 'marijuana', 'aaf': 'as a friend', 'aak': 'alive and kicking',
         'aamof': 'as a matter of fact', 'aap': 'always a pleasure', 'aar8': 'at any rate', 'aayf': 'always your friend',
         'abt': 'about', 'acc': 'anyone can come', 'adad': 'another day, another dollar', 'adn': 'any day now', 'any1': 'anyone',
         'af': 'as fuck', 'afaiaa': 'as far as I am aware', 'afaic': "as far as i'm concerned", 'afaik': 'as far as I know',
         'aight': 'alright', 'aka': 'also known as', 'ama': 'ask me anything', 'aml': 'all my love',
         'asap': 'as soon as possible', 'atm': 'at the moment', 'atys': 'anything you say', 'awa': 'as well as',
         'awc': 'after awhile crocodile', 'b4': 'before', 'b4n': 'bye for now', 'bau': 'business as usual',
         'bbl': 'be back later', 'bbs': 'be back soon', 'bc': 'because', 'b/c': 'because', 'bcnu': "be seein' you",
         'bd': 'big deal', 'bday': 'birthday', 'bf': 'boyfriend', 'bff': 'best friends forever',
         'bfn': 'bye for now', 'bif': 'before I forget', 'blnt': 'better luck next time',
         'bmg': 'be my guest', 'br': 'best regards', 'brb': 'be right back',
         'bro': 'brother', 'bta': 'but then again', 'btdt': 'been there, done that',
         'btfl': 'beautiful', 'btfo': 'back the fuck off', 'btfu': 'back the fuck up', 'btw': 'by the way',
         'btwn': 'between', 'bwdik': 'but what do I know', 'bytm': 'better you than me', 'c-p': 'sleepy',
         'cd9': 'code 9: parents nearby', 'chgr': 'charger', 'chk': 'check', 'cid': 'consider it done',
         'cmiiw': "correct me if I'm wrong", 'cos': 'change of subject', 'crb': 'come right back',
         'csl': "can't stop laughing", 'ctfu': 'cracking the fuck up', 'cu': 'see you', 'cul': 'see you later',
         'cul8r': 'see you later', 'cus': 'see you soon', 'cuz': 'because', 'cwot': 'complete waste of time',
         'cya': 'cover your ass', 'cye': 'check your e-mail', 'def': 'definitely', 'dfl': 'dying fucking laughing',
         'dfo': 'done fell out', 'diik': 'darned if I know', 'diy': 'do it yourself',
         'dkdc': "don't know, don't care", 'dqmot': "don't quote me on this", 'dtrt': 'do the right thing',
         'ducwim': 'do you see what I mean?', 'dw': "don't worry", 'dwbh': "don't worry, be happy",
         'dwf': 'divorced white female', 'dwm': 'divorced white male', 'dwnld': 'download', 'dwntn': 'downtown',
         'dyfi': 'did you find it?', 'e2eg': 'ear to ear grin', 'eod': 'end of discussion', 'eol': 'end of life',
         'eom': 'end of message', 'eor': 'end of rant', 'eta': 'estimated time of arrival', 'ez': 'easy',
         'f2f': 'face to face', 'f2t': 'free to talk?', 'fam': 'family', 'faq': 'frequently asked questions',
         'fb': 'facebook', 'fbm': 'fine by me', 'fbow': 'for better or worse', 'fcol': 'for crying out loud',
         'fimh': 'forever in my heart', 'fitb': 'fill in the blank', 'fml': 'fuck my life', 'foaf': 'friend of a friend',
         'fofl': 'falling on the floor laughing', 'fomcl': 'falling off my chair laughing', 'frfr': 'for real for real',
         'ftw': 'for the win', 'fubar': 'fouled up beyond all recognition', 'fwd': 'forward',
         'fwiw': "for what it's worth", 'fyeo': 'for your eyes only', 'fyi': 'for your information', 'g2cu': 'good to see you',
         'g2g': 'got to go', 'g2r': 'got to run', 'gac': 'get a clue', 'gb': 'goodbye',
         'gbtw': 'get back to work', 'gbu': 'god bless you', 'gd': 'good', 'gf': 'girlfriend', 'ggn': 'gotta go now',
         'gi': 'good idea', 'gj': 'good job', 'gl': 'good luck', 'gm': 'good morning',
         'gmta': 'great minds think alike', 'gnt': 'good night', 'gol': 'giggle out loud', 'gr8': 'great', 'gtg': 'got to go',
         'grwm': 'get ready with me', 'h8': 'hate', 'h/o': 'hold on', 'hagn': 'have a good night', 'hago': 'have a good one',
         'hak': 'hugs and kisses', 'hbd': 'happy birthday', 'hbu': 'how about you?',
         'hh': 'happy hour', 'hhis': 'hanging head in shame', 'hml': 'hate my life', 'hmu': 'hit me up',
         'hny': 'happy new year', 'hoas': 'hold on a second', 'hru': 'how are you?',
         'htb': 'heavens to betsy', 'hth': 'hope this helps', 'iac': 'in any case', 'iae': 'in any event', 'ic': 'i see',
         'icbw': 'it could be worse', 'idc': "I don't care", 'idgaf': "I don't give a fuck", 'idgi': "I don't get it",
         'idk': "I don't know", 'idm': "it doesn't matter", 'idts': "I don't think so", 'ig': 'instagram',
         'iirc': 'if I remember correctly', 'ik': 'I know', 'iktr': "I know that's right", 'ikr': 'I know, right',
         'ily': 'I love you', 'imho': 'in my humble opinion', 'imltho': 'in my less than humble opinion',
         'imo': 'in my opinion', 'impov': 'in my point of view', 'imu': 'I miss you', 'iow': 'in other words',
         'irl': 'in real life', 'jic': 'just in case', 'jk': 'just kidding', 'j/k': 'just kidding', 'jlmk': 'just let me know',
         'jmo': 'just my opinion', 'jp': 'just playing', 'jt': 'just teasing', 'j/t': 'just teasing',
         'jtlyk': 'just to let you know', 'jw': 'just wondering', 'k': 'okay', 'kis': 'keep it simple', 'kit': 'keep in touch',
         'kmp': 'keep me posted', 'kwim': 'know what I mean?', 'l8': 'late',
         'l8r': 'later', 'lbay': 'laughing back at you', 'lbh': "let's be honest",
         'lbs': 'laughing but seriously', 'llta': 'lots and lots of thunderous applause', 'lmirl': "let's meet in real life",
         'lmk': 'let me know', 'lol': 'laughing out loud',
         'lmao': 'laughing my ass off', 'lmbo': 'laughing my butt off', 'lml': 'love my life',
         'lqtm': 'laughing quietly to myself', 'ltns': 'long time no see', 'luv': 'love', 'ly2': 'love you too',
         'lyl': 'love you lots', 'lylab': 'love you like a brother', 'lylas': 'love you like a sister',
         'mcm': 'man crush monday', 'mirl': 'meet in real life',
         'mmb': 'message me back', 'moo': 'my own opinion', 'morf': 'male or female', 'mos': 'mom over shoulder',
         'msg': 'message', 'mtfbwu': 'may the force be with you', 'myob': 'mind your own business', 'mua': 'makeup artist',
         'mue': 'makeup enthusiasts', 'nalopkt': 'not a lot of people know that', 'nbd': 'no big deal', 'nc': 'no comment',
         'ne1': 'anyone', 'newb': 'person who is new to something', 'nfs': 'not for sale', 'nm': 'never mind',
         'nmh': 'not much here', 'nmp': 'not my problem', 'nmu': 'not much, you?', 'noob': 'inferior person',
         'n00b': 'inferior person', 'noyb': 'none of your business', 'np': 'no problem', 'nrn': 'no response necessary',
         'ns': 'not shit', 'nsfw': 'not safe for work', 'num': 'number', 'nvm': 'never mind', 'nw': 'no way',
         'nwo': 'no way out', 'nye': 'new years eve', 'o4u': 'only for you', 'oao': 'over and out', 'obvi': 'obviously',
         'o': 'oh', 'og': 'original gangster', 'oic': 'oh, I see', 'omdb': 'over my dead body',
         'omg': 'oh my god', 'omw': 'on my way', 'ook': 'oh okay', 'ooto': 'out of the office', 'ot': 'off topic',
         'otoh': 'on the other hand', 'otp': 'on the phone', 'otw': 'on the way', 'ottomh': 'off the top of my head',
         'owtte': 'or words to that effect', 'oyo': 'on your own', 'p911': 'parent in room', 'paw': 'parents are watching',
         'pcm': 'please call me', 'pdh': 'pretty darn happy', 'pedi': 'pedicure', 'pic': 'picture', 'pir': 'parent in room',
         'pita': 'pain in the ass', 'pls': 'please', 'plz': 'please', 'plu': 'people like us', 'pm': 'evening',
         'poc': 'point of contact', 'pov': 'point of view', 'ppl': 'people', 'proly': 'probably',
         'prw': 'parents are watching', 'ptmm': 'please tell me more', 'qt': 'cutie',
         'rbtl': 'read between the lines', 'rite': 'right', 'rofl': 'rolling on the floor laughing',
         'r': 'are', 'rl': 'real life', 'rly': 'really', 'rsn': 'real soon now', 'ruok?': 'are you okay?', 's2r': 'send to receive',
         'sc': 'snapchat', 'sfete': 'smiling from ear to ear', 'sis': 'sister', 'sit': 'stay in touch',
         'sitd': 'still in the dark', 'smh': 'shaking my head', 'soh': 'sense of humor',
         'somsw': 'someone over my shoulder watching', 'sosdd': 'same old stuff different day',
         'soz': 'sorry', 'srvc': 'service', 'sry': 'sorry', 'ssdd': 'same shit, different day', 'stfu': 'shut the fuck up',
         'str8': 'straight', 'sup': "what's up?", 'tafn': "that's all for now",
         'tba': 'to be announced', 'tbc': 'to be continued', 'tgbtg': 'to god be the glory', 'tbh': 'to be honest',
         'tc': 'take care', 'tff': 'too freaking funny', 'tfw': 'that feeling when', 'tgif': "thank goodness it's friday",
         'thx': 'thanks', 'tia': 'thanks in advance', 'tic': 'tongue in cheek', 'tisnf': 'that is so not fair',
         'tmb': 'text me back', 'tmi': 'too much information', 'tmr': 'tomorrow', 'tmrw': 'tomorrow', 'tott': 'think on these things',
         'ttfn': 'ta-ta for now', 'ttyl': 'talk to you later', 'ttyt': 'talk to you tomorrow',
         'txt': 'text', 'txtd': 'texted', 'ty': 'thank you', 'u': 'you', 'u2': 'you too', 'ur': "your",
         'vbg': 'very big grin', 'vday': 'valentines day', 'w8': 'wait', 'w/': 'with', 'w/e': 'whatever', 'w/o': 'without',
         'wb': 'welcome back', 'wbs': 'write back soon', 'wcw': 'woman crush wednesday', 'wdyt': 'what do you think',
         'wfm': 'works for me', 'wlcm': 'welcome', 'wolo': 'we only live once', 'wrg': 'wrong', 'wru': 'where are you?',
         'wtf': 'what the fuck', 'wtg': 'way to go', 'wth': 'what the heck?', 'wtr': 'water', 'wtvr': 'whatever',
         'wtw': 'what the what', 'wu': "what's up?", 'wya': 'where you at?', 'wyd': 'what you doing?',
         'wygowm': 'will you go out with me?', 'wysiwyg': 'what you see is what you get', 'wyrn': "what's your real name?",
         'xmas': 'christmas', 'xoxo': 'hugs and kisses', 'y': 'why', 'y2': 'you too', 'yolo': 'you only live once',
         'yrg': 'you are good', 'yt': 'youtube', 'yt?': 'you there?', 'yw': 'you are welcome', 'zzz': 'tired'}
        words = text.split(' ')
        replaced = []
        for word in words:
            try:
                replaced.append(SLANG_DICT[word])
            except KeyError:
                replaced.append(word)
        return ' '.join(replaced)

    def lower_case(self, text):
        """
        Returns an input string with all lower case characters.

        Args:
            text (str): String to be turned into lower case.

        Returns:
            str: Input string in lower case
        """
        return  text.lower()

#### OPERATIONS FOR MULTIPLE STRINGS
    def filter_by_sentence_length(self, texts, min_length=3):
        """
        Removes all sentences with less words than the required mininmum length.

        Args:
            texts (list): List of sentences.
            min_length (int): Minimum sentence length. Set to 3 per default.

        Returns:
            str: List containing only sentences with word number greater or equal to min_length.
        """
        return [sentence for sentence in texts if len(sentence.split(" "))>=min_length]

    def return_all_sentences(self, text_list):
        """
        Takes a list of strings and returns a list of strings with each element representing a sentence from the original strings.

        Args:
            text_list (list): List of strings to be split into sentences.

        Returns:
            list: list of strings with each element representinga single sentence.
        """
        all_sentences = []
        for text in text_list:
            sentences = self.split_into_sentences(self, text)
            all_sentences.append(sentences)
        return [sentence for sentence_list in all_sentences for sentence in sentence_list]

    def basic_clean_and_split(self, text_list):
        """
        For a list of sentences, removes all html entities and splits the sentences by smileys if possible.

        Args:
            text_list (list): List of strings to which the basic cleaning and splitting should be applied.

        Returns:
            list: Returns a list of strings without html entities with indiviudal elements for parts previously sperated by smileys.
        """
        basic_sentences = []
        for text in text_list:
            no_html = self.replace_html_entities(self, text)
            split_list = self.split_by_smileys(self, no_html)
            for elem in split_list:
                basic_sentences.append(elem)
        return basic_sentences

    def clean_sentence_list(self, sentences):
        """
        Applies the default cleaning pipeline to each element of an input list (of sentences).

        Args:
            sentences (list): List of sentences to be cleaned.

        Returns:
            list: list of cleaned sentences.
        """
        return [self.clean_pipeline(self, sentence) for sentence in sentences]

    def clean_and_filter(self, sentences):
        """
        Applies the default cleaning pipeline to each sentence of a sentence list and then filters that list by the default minimum sentence length removing possible duplicates.

        Args:
            sentences (list): List of sentences to be cleaned, filtered, and set.

        Returns:
            list: list of cleaned, filtered, and set sentences.
        """
        cleaned = self.clean_sentence_list(self, sentences)
        filtered = self.filter_by_sentence_length(self, cleaned)
        return list(set(filtered))

#### PIPELINE
    def clean_pipeline(self, data, functions=[remove_urls, remove_emojis, remove_hastags, normalise_quotationmarks, replace_slang, remove_numbers, remove_special_chars, remove_duplicate_punct, remove_excess_white_space, lower_case, strip]):
        """
        Sequentially applies a set of specified functions to the given data.

        Args:
            data (str): Data to be modified by function.
            functions (list): List of functions to be applied to the data, in sequential order (first applied first, second applied second, etc.)
                              The default is the following sequence: replace_html_entities, remove_urls, remove_emojis_smileys, remove_hastags, normalise_quotationmarks, replace_slang, remove_numbers, remove_special_chars, remove_duplicate_punct, remove_excess_white_space, lower_case, strip

        Returns:
            data (str): Data as modified by specified functions.
        """
        return reduce(lambda a, x: x(self, a),
                      functions,
                      data)

#### GENERAL CLEANING FUNCTION
    def load_clean_save(self, input_string, print_status=True, input_type=None):
        """
        Loads specified file from "corpus" directory, cleans and then saves it into the clean_data subdirectory.

        Args:
            input_string (str): file location within corpus directory
            print_status (boolean):  boolean value determining if status updates are to be printed, default: true
            input_type (str): specifies the input to type to apply correct cleaning functions (default: None, other possibilities: twss, erotica, sayings, subtitles)

        Returns:
            list: list of sentence strings
        """
        in_file = '../../corpus/'+input_string
        filename = input_string.split("/")[-1]
        out_file = '../../corpus/clean_data/'+input_string
        sentences = ''
        sentences_cleaned = ''

        if print_status:
            print("Cleaning {}...".format(filename))

        if filename.split('.')[1] == 'txt':
            sentences = self.load_sentences_from_txt(self, in_file)
        elif filename.split('.')[1] == 'csv':
            sentences = self.load_text_from_csv(self, in_file, 'text')

        if not input_type:
            sentences_cleaned = self.default_clean(self, sentences)

        elif input_type is 'subtitles':
            sentences_cleaned = self.clean_subtitles(self, sentences)

        elif input_type is 'twss':
            out_file = "../../corpus/clean_data/positive/twss.txt"
            twssstories_cleaned = self.clean_twsstories(self, sentences)
            #getting positive instances from The Office
            if print_status:
                print("Merging clean twssstories data with instances from the office...")
            in_file = '../../corpus/positive/theofficetwss.txt'
            office_twss = self.load_sentences_from_txt(self, in_file)
            office_twss_cleaned = self.clean_and_filter(self, office_twss)
            #getting original positive instances by Kiddon&Brun
            if print_status:
                print("Merging twssstories data with Kiddon&Brun (2011) corpus...")
            in_file = '../../corpus/positive/twss-stories-parsed.txt'
            original_twss = self.load_sentences_from_txt(self, in_file)
            original_twss_cleaned = self.clean_and_filter(self, original_twss)
            #merging all twss
            sentences_cleaned = list(set(twssstories_cleaned + original_twss_cleaned + office_twss_cleaned))

        elif input_type is 'sayings':
            out_file = "../../corpus/clean_data/negative/phrases_sayings.txt"
            sentences_cleaned = self.clean_sayings(self, sentences)

        elif input_type is 'erotica':
            out_file = "../../corpus/clean_data/erotic_sentences.txt"
            sentences_cleaned = self.clean_erotica(self, sentences)

        if sentences_cleaned is not '':
            self.write_list_to_file(self, sentences_cleaned, out_file)
            if print_status:
                print("Finished cleaning {}. Saved to {}".format(filename, out_file))
        else:
            raise Exception("The cleaning process returns no list data. Please double check your input parameters.")

    def default_clean(self, text_list):
        """
        Applies deafult clean functions basic_clean_and_split as well as clean_and_filter
        to a list of text lines.

        Args:
            text_list (list): text lines to be transformed into clean sentences

        Returns:
            list: List of clean sentences
        """
        sentences_basic = self.basic_clean_and_split(self, text_list)
        return self.clean_and_filter(self, sentences_basic)

#### FUNCTION EXCLUSIVE TO TWSSSTORIES CLEANING
    def clean_twsstories(self, twss_phrases):
        """
        Takes instances from twssstories.com and extracts the TWSS-triggering phrase.

        Args:
            twss_phrases (list): list of twssstories.com entries

        Returns:
            list: List of TWSS-triggering phrases for each element in list.
        """
        all_phrases = []
        for phrase in twss_phrases:
            phrase = self.clean_pipeline(self, phrase, [self.replace_html_entities, self.normalise_quotationmarks, self.strip])
            phrase = re.sub(r'TWWSS',r'TWSS', phrase)
            parts = phrase.split('"')
            parts = [part for part in parts if part]
            if 'TWSS' not in phrase:
                all_phrases.append((parts[-1]))
            else:
                all_phrases.append((parts[-2]))
        basic_clean = self.basic_clean_and_split(self, all_phrases)
        return self.clean_and_filter(self,basic_clean)

#### FUNCTION EXCLUSIVE TO SUBTITLE CLEANING
    def clean_subtitles(self, subtitles):
        subtitle_string = "".join([subtitle.replace("\n", " ") for subtitle in subtitles])
        subtitles_split = self.split_into_sentences(self, subtitle_string)
        return self.clean_and_filter(self, subtitles_split)

#### FUNCTION EXCLUSIVE TO PHRASES CLEANING
    def clean_sayings(self, sayings):
        """
        Cleans common English sayings by replacing noisy characters and reordering the saying if necessary.

        Args:
            sayings (list): list of text strings

        Returns:
            list: list of sentence strings
        """
        all_phrases = []
        for phrase_set in sayings:
            phrases = phrase_set.split('\n')
            for phrase in phrases:
                if " - " in phrase:
                    sides = phrase.split(" - ")
                    phrase = " ". join([sides[1],sides[0]])
                phrase = self.remove_excess_white_space(self,phrase)
                all_phrases.append(phrase)
        basic_clean = self.basic_clean_and_split(self, all_phrases)
        return self.clean_and_filter(self, basic_clean)

#### FUNCTIONS EXCLUSIVE TO EROTICA CLEANING
    NOT_TEXT_INDICATORS = ["copyright","©","(c)", "archive-", ".txt", ".doc", "disclaimer", "www.", "@",
                  "http", "this story may not be reproduced", "content-", "keywords", "add more chapters",
                  "download", "==", "multi-part message", "[]", "::", "the author may be contacted"
                  "to be continued", "repost", "written permission of the author.", "adding graphics", "this story",
                  "offended by stories of sex", "this file", "ascii", "column", "print", "top and bottom of each page"]
    def check_indicators(self, text, upper_bound_len, indicators=NOT_TEXT_INDICATORS):
        """
        Takes text elements and returns true if it includes indicators for headers or footers and if the element length is shorter than the upper bound.

        Args:
            text (list): string to be checked for indicators and length
            indicators (list): list of strings for which True is returned if they occur in a given element. Default is NOT_TEXT_INDICATORS list.
            upper_bound_len (int): int value that determines maximum length of element containing indicators for with True is returned

        Returns:
            bool: Boolean indicating if the given element contains the specified indicators and shorter than the given upper bound.
        """
        for element in self.NOT_TEXT_INDICATORS:
            if element in text.lower():
                if len(text)<upper_bound_len:
                    return True
        return False

    def split_erotica(self, erotica):
        """
        Splits erotica file by \n with an exception for especially formatted files that start every line with a "."

        Args:
            erotica (string): string of erotica text

        Returns:
            list: list of strings representing erotica text split by \n
        """
        try:
            if erotica[0] == '.':
                return erotica.split(" . ")
            else:
                return erotica.split("\n")
        except:
            return erotica.split("\n")

    def prune_erotica(self, erotica, upper_bound=4):
        """
        Prunes lines of erotic text by stripping them and removing all lines with less than 3 elements separated by " ".
        If the first element of a list obtained this way is shorter than 100 characters, it will be deleted.

        Args:
            erotica (list): list of strings
            upper_bound (int): int value that determines minimum length for tokens separted by " " in string elements

        Returns:
            list: pruned list of strings
        """
        pruned = [elem.strip() for elem in erotica if len(elem.strip().split(" "))>upper_bound]
        try:
            if len(pruned[0])<100:
                pruned.pop(0)
        except IndexError:
            pass
        return pruned

    def remove_irrelevant_content(self, erotic_texts):
        """
        Removes non-text content from erotica texts by applying various cleaning functions to a list of erotic texts.

        Args:
            erotic_texts (list): list of strings

        Returns:
            list: list of cleaned erotica texts
        """
        cleaned_texts = []
        for text in erotic_texts:
            piped = self.clean_pipeline(self, text, [self.remove_in_text_line_breaks, self.remove_tabs, self.remove_excess_white_space])
            split = self.split_erotica(self, piped)
            pruned = self.prune_erotica(self, split)
            no_indicators = [elem for elem in pruned if elem if self.check_indicators(self, elem, 300)==False]
            cleaned_text = ' '.join(no_indicators)
            cleaned_texts.append(cleaned_text)
        return cleaned_texts

    def clean_erotica(self, erotic_texts):
        """
        Cleans erotica texts from headers and footers, returning a list of individual erotic sentences from all texts.

        Args:
            erotic_texts (list): list of text strings

        Returns:
            list: list of sentence strings
        """
        texts_only = self.remove_irrelevant_content(self, erotic_texts)
        sentences = self.return_all_sentences(self, texts_only)
        basic_clean = self.basic_clean_and_split(self, sentences)
        cleaned_texts = self.clean_and_filter(self, basic_clean)
        return cleaned_texts
