from features import *
from classifier import *
from preprocessing import *
import sys
import pdb
import numpy as np

# Gets command parameters

classifier_name = sys.argv[1]
force_new_calculations = False
if(len(sys.argv)>2):
    force_new_calculations = sys.argv[2]=="recalculate"


positive_files = [
'corpus/clean_data/positive/common_phrases.txt',
'corpus/clean_data/positive/urbandictionary.txt',
'corpus/clean_data/positive/bodyphrases.txt',
'corpus/clean_data/positive/movieconversations.txt',
'corpus/clean_data/positive/twss.txt',
'corpus/clean_data/positive/phrases_sayings.txt'
]
negative_files = [
'corpus/clean_data/negative/bodyphrases.txt',
'corpus/clean_data/negative/common_phrases.txt',
'corpus/clean_data/negative/movieconversations.txt',
# 'corpus/clean_data/negative/SMSSpamCollection.txt',
# 'corpus/clean_data/negative/phrases_sayings.txt'
]

all_files = positive_files + negative_files
print("Combining corpora...")
combined_corpora_list = combine_corpora(all_files)
print("Reading erotica frecuencies...")
erotica_frecuencydict = erotica_managment.read_json_file('corpus/erotica_frecuencies_processed.json')
print("Creating featurizer...")

#Initilizes featurizer and corpus

featurizer = Featurizer.create_form_classes([
FeatureCharacterNgramsFun(combined_corpora_list),
FeaturePronounNgramsFun(),
FeatureSexyParentFun(erotica_frecuencydict),
FeatureSexinessFun(erotica_frecuencydict),
FeatureSexinessAppearanceFun(erotica_frecuencydict),
FeatureSexinessBinaryFun(erotica_frecuencydict),
FeatureUnsexyNounsNumberFun(erotica_frecuencydict),
FeatureTriggeringWordsFun(erotica_frecuencydict)
])
dict_corpus = featurize_corpus(positive_files, negative_files, featurizer, force_re_train=force_new_calculations)
print("Saving featurized corpus...")
erotica_managment.write_json_file('corpus/featurized_dict.json',dict_corpus)
corpus_train, corpus_test = split_data(dict_corpus)

## Recoverng or training classifier

if(classifier_name == "multinomial-bayes"):
    classifier = ClassifierMultinomialBayes.init_if_saved(featurizer,corpus_train,force_re_train=force_new_calculations)
if(classifier_name == "logistic-regression"):
    classifier = ClassifierLogisticRegression.init_if_saved(featurizer,corpus_train,force_re_train=force_new_calculations)
if(classifier_name == "support-vector-machine"):
    classifier = ClassifierSupportVectorMachine.init_if_saved(featurizer,corpus_train,force_re_train=force_new_calculations)


## Entering interactive menu


def evaluate():
    classifier.evaluate_with_test(corpus_test)

def proba_unanotated():
    print("Computing probabilities for unannotated files...")
    unanotated_files = [
    'corpus/clean_data/unannotated/sausage_party.txt',
    'corpus/clean_data/unannotated/lion_king.txt',
    'corpus/clean_data/unannotated/cinderella.txt',
    ]
    for file in unanotated_files:
        classifier.predict_to_file(file)
    print("Files generated")

def interactive():
    sentence = input("Please enter a sentence to predict or 'q' to go back to menu: ")
    prediction = classifier.predict(sentence, featurized=False)
    if prediction[0]==1:
        print("\n ----> That's what she said!!\n")
    else:
        print("\n ----> Boring!\n")

def interactive_info():
    sentence = input("Please enter a sentence to see all info for preditction (Cleaned sentence, probabilities and features): ")
    prediction = classifier.sentence_full_info(sentence)


def console():
    print("\nYou can now use the main variabels. Enter 'c' to go back to the menu")
    pdb.set_trace()

def weights():
    featurizer.pretty_print(classifier.get_weights())

def odd_ratio():
    distance = input("""
    Please enter a minimum distance.
    All odd ratios with smaller distance to 1 than the provided will be set to 0.0 and removed from the characted ngrams.
    Distance:
    """)
    featurizer.pretty_print(classifier.get_analized_weights(float(distance)))

def parameters():
    classifier.show_best_parameters()


switch = {
    '1': evaluate,
    '2': interactive,
    '3': proba_unanotated,
    '4': weights,
    '5': odd_ratio,
    '6': console,
    '7': parameters,
    '8': interactive_info
}

quit = False
while(not quit):
    print("""
    _______________________________________________________
                             MENU
    _______________________________________________________
    1) Evaluate with test data
    2) Enter phrases and see the prediction
    3) Generate probabilites for unanotated files
    4) Print weights
    5) Print odd ratio of features(Just Logistc Regression)
    6) Python console to interact with variables
    7) Show classifier best parameters
    8) Show probabilities and features for sentence

    Exit: q
    _______________________________________________________

    """)
    op = input('Option: ')
    if(op == 'q'):
        quit=True
    else:
        switch[op]()
