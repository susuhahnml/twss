from features import Featurizer
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import classification_report
from sklearn.model_selection import GridSearchCV
from preprocessing import Cleaner
import pickle
import os
import pandas as pd

"""
Classifier main class for TWSS stories
"""
class ClassifierTWSS(object):

    """
    Creates a classifier for TWSS stories
    """
    def __init__(self, featurizer, model):
        """
        Creates a ClassifierTWSS
        Args:
            featurizer: A class with a featurize function to transform text into
                features.
            model: The trained model
        """
        self.featurizer = featurizer
        self.model = model

    """
    Classifier for TWSS stories and trains it
    """
    @classmethod
    def init_train(cls, featurizer, corpus, parameter_matrix, sklearn_model):
        """
        Creates a classifier by computing the best parameters with
        Grid Search and cross validation and choses the optimal hyperparameters
        and creates the Classifier
        Args:
            featurizer: A class with a featurize function to transform text into
                features.
            corpus: Dictionary with the training corpus
            parameter_matrix : a list of different parameters from which the best are chosen
            sklearn_model : The sklearn model that will be used
        """
        bestmodel = GridSearchCV(estimator = sklearn_model, param_grid = parameter_matrix, cv=5, scoring= 'precision')
        bestmodel.fit(corpus['X'],corpus['Y'])
        return cls(featurizer,bestmodel)


    @classmethod
    def init_from_file(cls, featurizer):
        """
        Creates a classifier from a file containing the grid search with the best model
        """
        loaded_model = pickle.load(open('src/classifier/saved/'+cls.__name__+".sav", 'rb'))
        return cls(featurizer,loaded_model)


    def predict(self, sentence, featurized = True):
        """
        Predicts if a sentence is a TWSS

        Args:
            sentence: Can be a string or a featurized array.
        """

        if featurized:
            return self.model.predict(sentence)
        else:
            sentence = Cleaner().clean_pipeline(sentence)
            sentence = self.featurizer.featurize(sentence)
            predicted = self.model.predict([sentence])
            return predicted

    def sentence_full_info(self, sentence):
        """
        Shows the full info for a predicted sentence.
        The cleaned sentence, the features and the probability
        Args:
            sentence: An uncleaned phrase
        """
        sentence = Cleaner().clean_pipeline(sentence)
        print("Sentence cleaned: ", sentence)
        sentence = self.featurizer.featurize(sentence)
        print("Sentence features:\n")
        self.featurizer.pretty_print(sentence)
        predicted = self.model.predict([sentence])
        probability = self.model.predict_proba([sentence])
        print("TWSS probability: ", probability[0][1])
        print("NOT TWSS probability: ", probability[0][0])

    def twss_proba(self, sentence):
        """
        Gets the probability for being a TWSS sentence

        Args:
            Sentence: String to predict
        """
        sentence = Cleaner().clean_pipeline(sentence)
        sentence = self.featurizer.featurize(sentence)
        probability = self.model.predict_proba([sentence])
        string_proba = '{:.3f}'.format(probability[0][1])
        return float(string_proba)


    def show_best_parameters(self):
        """
        Prints the best hyperparameters found using the GridSearchCV
        """

        print('Hyperparameters of the best model: \n', self.model.best_estimator_.get_params())

    def get_weights(self):
        """
        Gets the weights of the trained_model
        Mirrors the feature_log_prob_(Empirical log probability of features given a class, P(x_i|y))
        for interpreting the model.

        Returns:
            array, shape (n_classes, n_features)
        """
        return self.model.best_estimator_.coef_[0]


    def evaluate(self, true_y, predicted_y):
        """
        Evaluate the trained model
        Args:
                Y values of the test sentences
                Predicted Y values for the test sentences
        Returns:
            precision : float (if average is not None) or array of float, shape = [n_unique_labels]
            recall : float (if average is not None) or array of float, , shape = [n_unique_labels]
            fbeta_score : float (if average is not None) or array of float, shape = [n_unique_labels]
            support : int (if average is not None) or array of int, shape = [n_unique_labels]
            The number of occurrences of each label in y_true.
        """
        score = precision_recall_fscore_support(true_y, predicted_y, average='weighted')
        report = classification_report(true_y, predicted_y, target_names = ['Not-TWSS', 'TWSS'])
        return report

    def save_model(self):
        """
        Saves the model in a file
        """
        filename = 'src/classifier/saved/'+self.__class__.__name__+".sav"
        pickle.dump(self.model, open(filename, 'wb'))

    @classmethod
    def init_if_saved(cls, featurizer, corpus_train, force_re_train=False):
        """
        If a saved model is found it recovers it. Otherwise it traines the a new model.
        """
        class_name = cls.__name__
        has_saved = os.path.exists('src/classifier/saved/'+cls.__name__+'.sav')
        if(force_re_train or not has_saved):
            print("Training model...")
            classifier = cls.init_train(featurizer,corpus_train)
            print(class_name + " trained")
            print("Saving model trained")
            classifier.save_model()
            print("Model saved")
        else:
            print("Recovering model from file...")
            classifier = cls.init_from_file(featurizer)
            print(class_name + " recovered from file")

        return classifier

    def evaluate_with_test(self, corpus_test):
        """
        Evaluates the classifier with the test corpus
        Args:
            corpus_test: The dictionary with the test corpus featurized in
                         the values X and Y
        """
        class_name = self.__class__.__name__
        predicted_y_NB = self.predict(corpus_test['X'], featurized = True)
        evaluate_score_NB = self.evaluate(true_y = corpus_test['Y'], predicted_y = predicted_y_NB)
        print(class_name+' evaluation with test corpus: \n', evaluate_score_NB)

    def predict_to_file(self, file_path):
        """
        Evaluates the classifier with the test corpus
        Args:
            file_path: The path of the file with the sentences
        """
        file_name=file_path.split('/')[-1]
        sentences = [s.strip('\n') for s in Cleaner().load_sentences_from_txt(file_path)]
        class_name = self.__class__.__name__
        probabilities = [self.twss_proba(s) for s in sentences]
        df = pd.DataFrame({"twss-probability":probabilities,"sentence": sentences})
        df.sort_values(by=['twss-probability'],ascending=False,inplace=True)
        df.to_csv("corpus/probabilities/"+class_name+"_probabilies_"+file_name+".csv",sep='\t')
