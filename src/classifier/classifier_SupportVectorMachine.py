from features import Featurizer
from sklearn.svm import LinearSVC
from classifier import ClassifierTWSS

"""
Classifier for TWSS stories using a support-vecotor machine
"""
class ClassifierSupportVectorMachine(ClassifierTWSS):

    @classmethod
    def init_train(cls, featurizer, corpus):
        """
        Creates a ClassifierSupportVectorMachine
        Args:
        featurizer: Used to transform sentence to features
        parameter matrix: A matrix of different parameters to find the optimals
        """
        # Expanded matrix. Uncomment for a bigger search
        # parameter_matrix = {'penalty': ['l2'], 'C': [1.0], 'tol': [0.001,0.0001], 'loss': ['hinge','squared_hinge'], 'fit_intercept': [True, False]}

        # Special matrix using best parameters for faster retrian
        parameter_matrix = {'penalty': ['l2'], 'C': [1.0], 'tol': [0.001], 'loss': ['squared_hinge'], 'fit_intercept': [False]}


        return super().init_train(featurizer, corpus, parameter_matrix, sklearn_model = LinearSVC())
