from features import Featurizer
from sklearn.linear_model import LogisticRegression
from classifier import ClassifierTWSS
import numpy as np

"""
Classifier for TWSS stories using Logistic Regression
"""
class ClassifierLogisticRegression(ClassifierTWSS):

    @classmethod
    def init_train(cls, featurizer, corpus):
        """
        Creates a ClassifierLogisticRegression
        Args:
        featurizer: Used to transform sentence to features
        parameter matrix: A matrix of different parameters to find the optimals
        """

        # Expanded matrix. Uncomment for a bigger search
        # parameter_matrix = {'C':np.logspace(-3,3,7), 'penalty':['l1','l2'], 'class_weight': ['balanced', None], 'solver': ['liblinear']}

        # Special matrix using best parameters for faster retrian
        parameter_matrix = {'C': [1.0], 'penalty':["l2"], 'solver': ['liblinear']}


        return super().init_train(featurizer, corpus, parameter_matrix, sklearn_model = LogisticRegression())

    def get_analized_weights(self,minimum = 0.15):
        """
        Returns odd ratio sending values with small distance to 1 to 0.
        Returns:
            array, shape (n_classes, n_features)
        """
        weights = self.get_weights()
        weights_odds_ratio = np.exp(weights)
        distance_condition = np.abs(1-weights_odds_ratio)>minimum
        final = np.where(distance_condition,weights_odds_ratio,0)
        return final
