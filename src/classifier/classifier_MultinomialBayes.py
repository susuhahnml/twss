from features import Featurizer
from sklearn.naive_bayes import MultinomialNB
from classifier import ClassifierTWSS
import numpy as np

"""
Classifier for TWSS stories using multinomial bayes
"""
class ClassifierMultinomialBayes(ClassifierTWSS):

    @classmethod
    def init_train(cls, featurizer, corpus):
        """
        Creates a ClassifierMultinomialBayes
        Args:
        featurizer: Used to transform sentence to features
        parameter matrix: A matrix of different parameters to find the optimals
        """
        parameter_matrix = {'alpha':[10**-x for x in range(1,7)]}
        return super().init_train(featurizer, corpus, parameter_matrix, sklearn_model = MultinomialNB())
