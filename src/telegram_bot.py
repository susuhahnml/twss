import telebot
from features import *
from classifier import *
from preprocessing import *
from collections import defaultdict
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, ConversationHandler
from telegram.ext import MessageHandler, Filters
import logging
import threading
import time

"""
Process to run the telegram bot
"""

all_files = [
'corpus/clean_data/positive/common_phrases.txt',
'corpus/clean_data/positive/urbandictionary.txt',
'corpus/clean_data/positive/bodyphrases.txt',
'corpus/clean_data/positive/movieconversations.txt',
'corpus/clean_data/positive/twss.txt',
'corpus/clean_data/positive/phrases_sayings.txt',
'corpus/clean_data/negative/common_phrases.txt',
'corpus/clean_data/negative/movieconversations.txt',
'corpus/clean_data/negative/bodyphrases.txt'
# 'corpus/clean_data/negative/SMSSpamCollection.txt',
]

combined_corpora_list = combine_corpora(all_files)
erotica_frecuencydict = erotica_managment.read_json_file('corpus/erotica_frecuencies_processed.json')
featurizer = Featurizer.create_form_classes([
FeatureCharacterNgramsFun(combined_corpora_list),
FeaturePronounNgramsFun(),
FeatureSexyParentFun(erotica_frecuencydict),
FeatureSexinessFun(erotica_frecuencydict),
FeatureSexinessAppearanceFun(erotica_frecuencydict),
FeatureSexinessBinaryFun(erotica_frecuencydict),
FeatureUnsexyNounsNumberFun(erotica_frecuencydict),
FeatureTriggeringWordsFun(erotica_frecuencydict)
])
classifier = ClassifierLogisticRegression.init_from_file(featurizer)




## Initial definitions
users_pronouns = defaultdict(lambda: 'she')
users_thresholds = defaultdict(lambda: 50)
TELEGRAM_HTTP_API_TOKEN = '717761761:AAFCmzTzXGR5_V0jv62NYXKGugzenynTmqo'
updater = Updater(TELEGRAM_HTTP_API_TOKEN)
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.WARN)
lck = threading.Lock()
not_twss = set()


FIRST, SECOND = range(2)


def start(bot, update):
    """
    Command for bot starting
    """
    intro_text = """
    I will answer *Thats what she said* to your messages when you least expect it.

    - Use */set_pronoun* if you want to change the pronoun I use.
    - Use */set_threshold* if you want to change how certain the bot has to be to answer twss. Default set to 50%, a higher value will make the bot less noisy.
    - Reply to any message with *twss?*, if you want to to know how close it was to trigger me.
    - Reply to any message with *twss*, to teach me a twss phrase I missed.
    - Reply to any message with *bad bot*, to teach me I was wrong to answer twss.

    Enjoy \N{smirking face}
    """
    bot.send_message(chat_id=update.message.chat_id, text=intro_text,parse_mode='Markdown')

def set_pronoun(bot, update):
    """
    Command to show proboun options
    """
    keyboard = [
    [InlineKeyboardButton('She', callback_data='she')],
    [InlineKeyboardButton('He', callback_data='he')],
    [InlineKeyboardButton('They', callback_data='they')]]

    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.send_message(chat_id=update.message.chat_id, text="Chose your prefered pronoun: ", reply_markup=reply_markup)
    return FIRST

def set_threshold(bot, update):
    """
    Command to show proboun options
    """
    keyboard = [
    [InlineKeyboardButton('30%', callback_data='30%'),InlineKeyboardButton('40%', callback_data='40%'),InlineKeyboardButton('50%', callback_data='50%')],
    [InlineKeyboardButton('60%', callback_data='60%'),InlineKeyboardButton('65%', callback_data='65%')],
    [InlineKeyboardButton('70%', callback_data='70%'),InlineKeyboardButton('75%', callback_data='75%')],
    [InlineKeyboardButton('80%', callback_data='80%'),InlineKeyboardButton('85%', callback_data='85%')],
    [InlineKeyboardButton('90%', callback_data='90%'),InlineKeyboardButton('95%', callback_data='95%')],
    ]

    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.send_message(chat_id=update.message.chat_id, text="Chose your prefered threshold, a higher value will make me less noisy: ", reply_markup=reply_markup)

def prob_to_emogi(probability):
    """
    Transforms the probability to emogi
    """
    emogi = ""
    if(probability<30):
        emogi = '\N{smiling face with halo}'
    elif(probability<60):
        emogi = '\N{winking face}'
    elif(probability<90):
        emogi = '\N{smirking face}'
    elif(probability>90):
        emogi = '\N{smiling face with horns}'
    return emogi

def twss_predict(bot, update):
    """
    Function runed for every message. Will predict a possible TWSS or get the probability
    """
    message = update.message
    is_old = int(time.time()) > int(message.date.timestamp()) +3
    if(is_old):
        return
    if(message.reply_to_message is not None):
        if('twss?' in message.text.lower()):

            ###### GET PRECISION

            text = message.reply_to_message.text
            probability = int(100*classifier.twss_proba(text))
            # print("Predicting: (%d) %s" %(probability,text))
            bot.send_message(message.chat.id, prob_to_emogi(probability)+(' I am *{}%* sure thats what '+users_pronouns[message.chat.id]+' said').format(probability), parse_mode='Markdown',reply_to_message_id=message.reply_to_message.message_id)
        else:
            if('twss' in message.text.lower()):

                ###### ADD POSITIVE

                text = message.reply_to_message.text
                learn_sentence(text, True)
                em = '\N{face with tears of joy}'
                probability = int(100*classifier.twss_proba(text))
                if(probability>50):
                    bot.send_message(message.chat.id, em + " " + em + " " + " I knew it! Is " + str(probability) + "%", parse_mode='Markdown',reply_to_message_id=message.reply_to_message.message_id)
                else:
                    bot.send_message(message.chat.id, em + " " + em + " " + "Good one! I will learn that...", parse_mode='Markdown',reply_to_message_id=message.reply_to_message.message_id)
            if('bad bot' in message.text.lower()):

                ###### ADD NEGATIVE

                text = message.reply_to_message.text
                was_my_reply =  "That's what " in text
                em = '\N{slightly frowning face}'

                if(was_my_reply):
                    bot.send_message(message.chat.id, "Sorry about that " + em + " Please reply the same to the original message (the one I aswered) so I can learn from my mistakes", parse_mode='Markdown',reply_to_message_id=message.message_id)
                else:
                    probability = int(100*classifier.twss_proba(text))
                    if(probability<50):
                        em = '\N{unamused face}'
                        bot.send_message(message.chat.id, "I already knew that is not a twss phrase! " + em + " Is only " + str(probability)  +"%", parse_mode='Markdown',reply_to_message_id=message.message_id)
                    else:
                        learn_sentence(text, False)
                        not_twss.add(text)
                        bot.send_message(message.chat.id, "Sorry about that " + em + " I will learn that..", parse_mode='Markdown',reply_to_message_id=message.message_id)


    else:

        ###### PREDICT

        text = message.text
        probability = int(100*classifier.twss_proba(text))
        is_TWSS = probability> users_thresholds[message.chat.id]
        is_TWSS = is_TWSS and not (text in not_twss)
        # print("Predicting: (%d) %s" %(probability,text))
        len_text = len(message.text.split())
        if is_TWSS and len_text > 2 and len_text < 15:
            bot.send_message(message.chat.id, "That's what "+users_pronouns[message.chat.id]+" said! " + prob_to_emogi(probability),reply_to_message_id=message.message_id)



def set_pronoun_ac(bot, update):
    """
    Function to change the chat pronoun
    """
    id = update.callback_query.message.chat.id
    users_pronouns[id]=update.callback_query.data
    bot.send_message(id, "Pronoun set to: " +update.callback_query.data )

def set_threshold_ac(bot, update):
    """
    Function to change the threshold
    """
    id = update.callback_query.message.chat.id
    new_t = int(update.callback_query.data[0:2])
    users_thresholds[id]= new_t
    bot.send_message(id, "Threshold set to: " +update.callback_query.data )


def learn_sentence(sentence, is_positive):
    """
    Adds the sentence to the corpus for the specific class.
    """
    lck.acquire()
    class_type = "positive" if is_positive else "negative"
    file_path = "./corpus/" + class_type + "/telegram.txt"
    with open(file_path, 'a') as f:
        f.write(sentence + '\n')
    lck.release()


def bad_bot_ac(bot, update):
    """
    Function to correct the bot
    """
    id = update.callback_query.message.chat.id
    new_t = int(update.callback_query.data[0:2])
    users_thresholds[id]= new_t
    bot.send_message(id, "Threshold set to: " +update.callback_query.data )

updater = Updater(TELEGRAM_HTTP_API_TOKEN)
start_handler = CommandHandler('start', start)
set_pronoun_handler = CommandHandler('set_pronoun', set_pronoun)
set_threshold_handler = CommandHandler('set_threshold', set_threshold)
twss_handler = MessageHandler(Filters.text, twss_predict)
pronoun_handler = CallbackQueryHandler(set_pronoun_ac,pattern='^she$^|he$|^they$')
threshold_handler = CallbackQueryHandler(set_threshold_ac,pattern='^\d*[%]$')
updater.dispatcher.add_handler(start_handler)
updater.dispatcher.add_handler(set_pronoun_handler)
updater.dispatcher.add_handler(set_threshold_handler)
updater.dispatcher.add_handler(twss_handler)
updater.dispatcher.add_handler(pronoun_handler)
updater.dispatcher.add_handler(threshold_handler)

updater.start_polling(poll_interval = 1.0,timeout=20)
