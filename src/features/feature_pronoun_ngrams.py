from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

all_pronoun = ['I', 'you', 'he', 'she', 'it', 'we', 'they', 'me', 'him', 'her', 'us', 'them', 'I', 'you', 'he', 'she', 'it', 'we', 'they', 'what', 'who', 'me', 'him', 'her', 'it', 'us', 'you', 'them', 'whom', 'mine', 'yours', 'his', 'hers', 'ours', 'theirs', 'this', 'that', 'these', 'those', 'who', 'whom', 'which', 'what', 'whose', 'whoever', 'whatever', 'whichever', 'whomever', 'who', 'whom', 'whose', 'which', 'that', 'what', 'whatever', 'whoever', 'whomever', 'whichever', 'myself', 'yourself', 'himself', 'herself', 'itself', 'ourselves', 'themselves', 'myself', 'yourself', 'himself', 'herself', 'itself', 'ourselves', 'themselves', 'anything', 'everybody', 'another', 'each', 'few', 'many', 'none', 'some', 'all', 'any', 'anybody', 'anyone', 'everyone', 'everything', 'no', 'one', 'nobody', 'nothing', 'none', 'other', 'others', 'several', 'somebody', 'someone', 'something', 'most', 'enough', 'little', 'more', 'both', 'either', 'neither', 'one', 'much', 'such', 'each other', 'one another']

"""
Function to get the FeaturePronounNgramsFun Class
Args:
    pronounlist: List of all pronouns
"""
def FeaturePronounNgramsFun(pronounlist = all_pronoun):


    class FeaturePronounNgrams(object):

        def __init__(self):
            """
            Initilizes the count vectorizer
            """
            self.set_parameters()


        def set_parameters(self):
            """
            Sets the parameters for the vectorizer and fits the countvectorizer to a given list of pronouns
            """
            self.vectorizer = CountVectorizer(decode_error='ignore', ngram_range=(1,1), analyzer='word')
            self.vectorizer.fit(pronounlist)
            self.number_of_features= len(self.vectorizer.get_feature_names())

            return


        def featurize(self, sentence, extra={}):
            """
            Transform documents to document-term matrix.
            Extract token counts out of raw text documents using the vocabulary fitted with fit
            of the one provided to the constructor.

            Args:
                A string to transform
            Returns:
                An array containing pronoun ngrams converted to a vector
            """
            vector = self.vectorizer.transform([sentence])
            return vector.toarray()[0].tolist()


        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                self.number_of_features
            """
            features_names = self.vectorizer.get_feature_names()
            positive_pronouns = {features_names[i]:f for i,f in enumerate(features) if f>0}
            return positive_pronouns

    return FeaturePronounNgrams
