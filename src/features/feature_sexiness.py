"""
Gets a feature of the sexiness rate of the verbs nouns and adjectives.
Args:
    erotica_frecuency: A dictionary with the erotica frecuencies

"""
def FeatureSexinessFun(erotica_frecuency):
    class FeatureSexiness(object):

        def __init__(self, data=[]):
            """
            Creates a feature given the data.
            To generate the rating, will use the default erotica corpus.
            """
            self.frecuencies=erotica_frecuency
            self.postags=erotica_frecuency.keys()
            self.postags_indexs={pos:i for i,pos in enumerate(self.postags)}
            self.number_of_features=len(self.postags)


        def featurize(self, sentence, extra={}):
            """
            Transforms the sentence into a list of numeric features.
            Args:
                sentence: The sentence to transform
            Returns:
                A list with three elements, the sexiness rate for verb, adj and noun.
            """
            if(not "sentence_taged" in extra):
                print(extra)
                raise ValueError('Expected an argument sentence_taged in the extra dictionary')
            ratings=[0]*self.number_of_features
            ocurrences=[0]*self.number_of_features
            for token in extra["sentence_taged"]:
                if(not token.pos_ in self.postags):
                    continue
                ocurrences[self.postags_indexs[token.pos_]]+=1
                if(token.lemma_ in self.frecuencies[token.pos_]):
                    ratings[self.postags_indexs[token.pos_]]+=self.frecuencies[token.pos_][token.lemma_]

            for i in range(self.number_of_features):
                if(ocurrences[i]!=0):
                    ratings[i]= int(ratings[i]/ocurrences[i])

            return ratings

        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                    self.number_of_features
            """
            return { pos+"_frecuency_ratio":features[i] for i, pos in enumerate(self.postags) }

    return FeatureSexiness
