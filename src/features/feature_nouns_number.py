from collections import defaultdict

"""
Gets a FeatureUnsexyNounsNumberFun class with the number of unsexy nouns.
Args:
    erotica_frecuency: A dictionary with the erotica frecuencies
"""
def FeatureUnsexyNounsNumberFun(erotica_frecuency):

    class FeatureUnsexyNounsNumber(object):

        def __init__(self, data=[]):
            """
            Creates a feature given the data.
            """
            self.frecuencies=erotica_frecuency
            self.number_of_features=1


        def featurize(self, sentence, extra={}):
            """
            Transforms the sentence into a list of numeric features.
            Args:
                sentence: The sentence to transform
            Returns:
                A list with one element indicating the number of unsexy nouns
            """
            if(not "sentence_taged" in extra):
                print(extra)
                raise ValueError('Expected an argument sentence_taged in the extra dictionary')
            unsexy_nouns=0
            for token in extra["sentence_taged"]:
                if(token.pos_ != "NOUN"):
                    continue
                if(not token.lemma_ in self.frecuencies["NOUN"]):
                    unsexy_nouns+=1

            return [unsexy_nouns]

        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                    self.number_of_features
            """
            return {"number_of_unsexy_nouns": features[0]}

    return FeatureUnsexyNounsNumber
