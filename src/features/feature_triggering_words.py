from collections import defaultdict


"""
Gets a feature with the number of trigering words, boring words and unwanted words
Args:
    erotica_frecuency: A dictionary with the erotica frecuencies

"""
def FeatureTriggeringWordsFun(erotica_frecuency):
    class FeatureTriggeringWords(object):

        def __init__(self, data=[]):
            """
            Creates a feature given the data.
            """
            self.frecuencies=erotica_frecuency
            self.number_of_features=3


        def featurize(self, sentence, extra={}):
            """
            Transforms the sentence into a list of numeric features.
            Args:
                sentence: The sentence to transform
            Returns:
                A list with one element indicating the number of unsexy nouns
            """
            if(not "sentence_taged" in extra):
                print(extra)
                raise ValueError('Expected an argument sentence_taged in the extra dictionary')

            def isTriggeringWord(token):
                best_pos=['PRON','DET']
                sexual_pos=['VERB','NOUN','ADJ']
                if token.pos_ in best_pos:
                    return True
                if token.pos_ in sexual_pos:
                    if token.lemma_ in self.frecuencies[token.pos_]:
                        return True
                return False

            def isBoringWord(token):
                boring_pos=['ADP','AUX','CCONJ','INTJ','PART','PUNCT','SCONJ','SYM','X']
                return token.pos_ in boring_pos

            triggering_words=0
            unwanted_words=0
            boring_or_triggering_words=0
            for token in extra["sentence_taged"]:
                triggers = isTriggeringWord(token)
                boring = isBoringWord(token)
                unwanted_word = (not triggers) and (not boring)
                #print("%s(%s) triggers:%r, boring:%r, unwanted:%r"%(token.text,token.pos_,triggers,boring,unwanted_word))
                if(boring or triggers):
                    boring_or_triggering_words+=1
                if(triggers):
                    triggering_words+=1
                if(unwanted_word):
                    unwanted_words+=1

            return [triggering_words,
            unwanted_words,
            boring_or_triggering_words
            ]

        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                    self.number_of_features
            """
            return {
            "number_of_triggering_words": features[0],
            "number_of_unwanted_words": features[1],
            "number_of_boring_or_triggering_words": features[2]
            }

    return FeatureTriggeringWords
