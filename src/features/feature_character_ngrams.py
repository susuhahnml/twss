from sklearn.feature_extraction.text import CountVectorizer
import numpy as np

"""
Function to get the FeatureCharacterNgrams Class
Args:
    corpus: List of all sentences in the corpus
"""
def FeatureCharacterNgramsFun(corpus):


    class FeatureCharacterNgrams(object):
        """
        Featurizes character ngrams of the whole corpus
        """

        def __init__(self):
            """
            Initilizes the parameters for the countvectorizer
            """
            self.ngrammin = 4
            self.ngrammax = 4
            self.min_df = 1
            self.max_df = 1.0
            self.set_parameters()


        def set_parameters(self):
            """
            Sets the parameters for the vectorizer and fits the vectorizer to the whole corpus
            """
            self.vectorizer = CountVectorizer(decode_error='ignore', ngram_range=(self.ngrammin, self.ngrammax), analyzer='char_wb', max_df=self.max_df, min_df=self.min_df)
            self.vectorizer.fit(corpus)
            self.number_of_features= len(self.vectorizer.get_feature_names())

            return


        def featurize(self, sentence, extra={}):
            """
            Transform documents to document-term matrix.
            Extract token counts out of raw text documents using the vocabulary fitted with fit
            or the one provided to the constructor.

            Args:
                sentence: The string to transform
            Returns:
                An array containing character ngrams of a string converted to a vector
            """
            vector = self.vectorizer.transform([sentence])

            return vector.toarray()[0].tolist()


        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                self.number_of_features
            """
            features_names = self.vectorizer.get_feature_names()
            positive_grams = {features_names[i]:f for i,f in enumerate(features) if f>0}
            return positive_grams

    return FeatureCharacterNgrams
