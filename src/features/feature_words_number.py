"""
Gets a feature of the number of words in the sentence
"""
class FeatureWordsNumber(object):

    def __init__(self, data=[]):
        """
        Creates a feature given the data.
        """
        self.number_of_features=1

    def featurize(self, sentence,extra={}):
        """
        Transforms the sentence into an list of numeric features.
        Args:
            sentence: The sentence to transform
        Returns:
            A list with one element, the number of words of the sentence
        """
        return [len(sentence.split())]

    def get_info(self, features):
        """
        Gets the information of the feature list in a human readable format
        Args:
            features: The list of features for the specific Feature class. Of size
                self.number_of_features
        """
        return {"number_of_words":features[0]}
