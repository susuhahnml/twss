import json
from .feature_words_number import FeatureWordsNumber
from .feature_character_ngrams import FeatureCharacterNgramsFun
import spacy
nlp = spacy.load('en_core_web_sm')


"""
Class to trasform sentences into an list of features.
Attribute:
    fetaure_classes: The list of feature classes that are going to be used
"""
class Featurizer(object):

    def __init__(self, features):
        self.features = features

    """
    Creates instances of all features_clases and uses them to contruct the featurizer
    Args:
        features_clases: The clases of the features to create
    """
    @classmethod
    def create_form_classes(cls, features_classes):
        features = [F() for F in features_classes]
        return cls(features)


    """
    Transforms a sentence into an list of features
    Args:
        sentence: The sentence to transform
    Returns:
        An list of features numbers
    """
    def featurize(self, sentence):
        ## TODO: Pass sentence trough clean process
        featurized_sentence = []
        sentence_taged = nlp(sentence)
        for feature in self.features:
            featurized_sentence.extend(feature.featurize(sentence,{"sentence_taged":sentence_taged}))
        return featurized_sentence


    """
    Gets an dictionary with the information of the featureize function
    Args:
        features_list: The list of features representing the string
    Returns:
        A dictionary with the information of each feature
    """
    def get_all_features_info(self, features_list):
        all_info = {}
        index = 0
        for feature in self.features:
            new_index = index + feature.number_of_features
            all_info[feature.__class__.__name__]=feature.get_info(features_list[index:new_index])
            index = new_index
        return all_info


    """
    Gets the info of an especific feature
    Args:
        features_list: The list of features representing the string
        Feature: The feature class to get the info from
    """
    def get_one_feature_info(self, features_list, Feature):
        index = 0
        for feature in self.features:
            if feature.__class__ == Feature:
                return feature.get_info(features_list[index:index + feature.number_of_features])
            index+=feature.number_of_features

    """
    Prints the features dictionary in a readable way
    Args:
        features_list: The list of features representing the string
    """
    def pretty_print(self, features_list):
        print("\n")
        print(json.dumps(self.get_all_features_info(features_list), indent = 4))
