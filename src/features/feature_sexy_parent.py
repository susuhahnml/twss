from collections import defaultdict

"""
Gets a feature with the number of pronons that have a sexy parent or sibling
Args:
    erotica_frecuency: A dictionary with the erotica frecuencies

"""
def FeatureSexyParentFun(erotica_frecuency):
    class FeatureSexyParent(object):

        def __init__(self, data=[]):
            """
            Creates a feature given the data.
            """
            self.frecuencies=erotica_frecuency
            self.number_of_features=1


        def featurize(self, sentence, extra={}):
            """
            Transforms the sentence into a list of numeric features.
            Args:
                sentence: The sentence to transform
            Returns:
                A list with one element indicating the number of pronouns
                with sexy parents
            """
            if(not "sentence_taged" in extra):
                raise ValueError('Expected an argument sentence_taged in the extra dictionary')
            def isSexy(token):
                sexual_pos=['VERB','NOUN','ADJ']

                if(token.pos_ not in sexual_pos):
                    return False
                return token.lemma_ in self.frecuencies[token.pos_]


            count_sexy_parents=0
            for token in extra["sentence_taged"]:
                parent = token.head
                if(token.pos_ != "PRON"):
                    continue

                if(isSexy(parent)):
                    count_sexy_parents+=1
                else:
                    if any(isSexy(c) for c in parent.children):
                        count_sexy_parents+=1



            return [count_sexy_parents]

        def get_info(self, features):
            """
            Gets the information of the feature list in a human readable format
            Args:
                features: The list of features for the specific Feature class. Of size
                    self.number_of_features
            """
            return {"number_pronouns_with_sexy_parents": features[0]}

    return FeatureSexyParent
